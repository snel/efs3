.. index:: intro

Introduction and installation
===============================

Features
--------
This application provides an efficient Python implementation of the Primary Energy Module (PEM) and a Multi-Objective Optimiser (MOO) for it.
The application is highly configurable and supports parallel execution to take advantage of multiple CPU cores.

Installation
------------
EFS requires Python 3.5. There are two distributions you can choose from that have been tested to work on Windows 7+.

* CPython--the official Python distribution.

  1. Download `CPython 3.5.2 <https://www.python.org/downloads/release/python-352/>`_.

  2. Runtime dependencies should be installed automatically by the EFS installer. To install them manually, do the following:
  
    1. Open terminal with Administrator privileges--click `start`, type `cmd`, right-click on `cmd.exe` and select `run with Administrator privileges`.
    2. Upgrade PIP--in terminal, enter:
      ```python -m pip install --upgrade pip```
    3. Install numpy and other packages--in terminal, enter:
      ```pip install numpy pandas click pyqt5```

* WinPython--portable Python distribution.
 
  1. Download `WinPython 3.5.2 <https://sourceforge.net/projects/winpython/files/latest/download?source=files>`_. All required dependencies are included.

Once Python has been installed, run the EFS installer (`setup_efs_3.0.exe`) *with administrator privileges* to ensure successful installation of dependencies. 
  
.. note::
   If you get the following error when installing Python: 
   
      ```The program can't start because api-ms-win-crt-runtime-l1-1-0.dll is missing from your computer. Try reinstalling the program to fix this problem``` 
   then you are missing a Microsoft Visual C++ redistributable. You can download it for your version of Windows `here <https://support.microsoft.com/en-us/kb/2999226>`_.

.. note::
   The installer is only designed to work on Windows. EFS itself runs on any Python 3-compatible operating system. The R delegation feature requires the R compiler to be installed.
  
Running EFS
-----------
The installer provides shortcuts to the main script (`efs.py`).

The main script accepts two optional command-line parameters:

--config     Path to JSON configuration file. Default is "```./config.json```".
--gui        Enables the GUI. Default is "`True`".

If you encounter problems writing to the output file, try specifying an output path in your user directory instead (e.g. ```c:/users/john/output.csv```). You can also try running EFS with administrator privileges.

Graphical User Interface (GUI)
------------------------------
The GUI provides a `File` menu and a `Start MOO CEM` button.
The `File` menu allows one to open a configuration file than the currently selected one. 
The `Start MOO CEM` button begins computation using the selected configuration.
A progress bar will appear. Once computation is complete, a notification will appear on screen and results will be written to the output file.

If the user cancels the computation, a similar notification will appear and possibly sub-optimal results will be written to the output file.