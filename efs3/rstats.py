"""
Python equivalents to R stats functions.
"""

import numpy as np


def rnorm(n: int, mean: float, sd: float):
    """
    Returns samples from a normal distribution.

    Args:
        n: Number of samples.
        mean: Mean value.
        sd: Standard deviation.

    Returns:
        `n` samples.
    """
    return np.random.normal(mean, sd, n)


def rtruncnorm(N: int, a: float, b: float, mean: float, sd: float):
    """
    Returns samples from a truncated normal distribution.

    Args:
        n: Number of samples.
        mean: Mean value.
        sd: Standard deviation.

    Returns:
        `n` samples.
    """
    # We could use scipy, as follows:
    # return scipy.stats.truncnorm.rvs(
    #     (a - mean) / sd, (b - mean) / sd, loc=mean, scale=sd, size=N)
    # But this is equivalent and removes the scipy dependency.
    if sd == 0:
        return np.zeros(N)
    else:
        return np.clip(np.random.normal(mean, sd, N), a_min=a, a_max=b)


def rlogis(n: int, location: float, scale: float):
    """
    Returns samples from a logistic distribution.

    Args:
        n: Number of samples.
        location: Mean value.
        scale: Standard deviation.

    Returns:
        `n` samples.
    """
    return np.random.logistic(location, scale, n)


def rtriangle(n: int, a: float, b: float, mode):
    """
    Returns samples from a triangular distribution.

    Args:
        n: Number of samples.
        a: Lower bound.
        b: Upper bound.
        mode: Value where the peak of the distribution occurs.

    Returns:
        `n` samples.
    """
    return np.random.triangular(a, mode, b, n)


def rweibull3(n: int, shape, scale: float, thres: float):
    """
    Returns samples from a Weibull distribution.

    Args:
        n: Number of samples.
        shape: Output shape.
        scale: Scale factor.
        thre: Threshold value.

    Returns:
        `n` samples.
    """
    return thres + scale * np.random.weibull(shape, n)


def rlnorm3(n: int, shape, scale: float, thres: float):
    """
    Returns samples from a log-normal distribution.

    Args:
        n: Number of samples.
        scale: Scale factor.
        thre: Threshold value.

    Returns:
        `n` samples.
    """
    return thres + np.random.lognormal(shape, scale, n)


def rt(n: int, df: int):
    """
    Returns samples from a standard t-distribution.

    Args:
        n: Number of samples.
        df: Degrees of freedom.

    Returns:
        `n` samples.
    """
    return np.random.standard_t(df, n)


def runif(n: int, a, b):
    """
    Returns samples from a uniform distribution.

    Args:
        n: Number of samples.
        mean: Mean value.
        sd: Standard deviation.

    Returns:
        `n` samples.
    """
    # Optimization to avoid returning an array -- but MOO fails with this :(.
    # if n == 1:
    #     return a + (b - a) * np.random.rand()
    # else:
    return np.random.uniform(a, b, n)


def rBeta_ab(n: int, shape1, shape2, a: float, b: float):
    r"""
    Returns samples (random deviates) from a 4-parameter Beta distribution.
    Equivalent to R's `rBeta_ab` function, defined as 
    
    .. math::
    
       f(x) = \frac{1}{B(p,q)} \frac{(x-a)^{(p-1)})(b-x)^{(q-1)}}{((b-a)^{(p+q-1)}))}.

    Args:
        n: Number of samples to return.
        shape1: Positive shape parameter; `p` in the definition of `f(x)` above.
        shape2: Positive shape parameter; `q` in the definition of `f(x)` above.
        a: Boundary parameter; `a` in the definition of `f(x)` above.
        b: Boundary parameter; `b` in the definition of `f(x)` above.

    Returns:
        Random deviates from the Beta distribution.

    See:
        The original R code for the Beta_ab function:
        [https://github.com/cran/ExtDist/blob/master/R/Beta_ab.R]
    """
    x = np.random.beta(a=shape1, b=shape2, size=n)
    return (b - a) * x + a

    # We can also do this using SciPy, but it's slower.
    # Source: https://people.duke.edu/~ccc14/sta-663/MonteCarlo.html
    # import scipy.stats as ss
    # rv = ss.beta(a=a, b=b)
    # rv.rvs(num_vars)