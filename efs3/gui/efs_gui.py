"""
GUI for EFS3.
"""
import sys
import threading

import numpy as np
from PyQt5.QtCore import QTimer
from PyQt5.QtCore import pyqtSignal
from PyQt5.QtWidgets import QAction, QApplication, QPushButton, QFileDialog, QMainWindow, QMessageBox, QProgressDialog

import efs
from efs3.gui.time_estimator import TimeEstimator
from efs3.efs_config import EFSConfig


class GUI(QMainWindow):
    """GUI for EFS3."""

    _signal = pyqtSignal()
    """Signal for closing the progress dialog."""  # PyQt requires this to be set at class-level.

    def __init__(self, config: EFSConfig):
        """
        Initializes a `GUI` object.

        Args:
            config: `EFSConfig` object.
        """
        super().__init__()
        self.config = config
        self.pem, self.moo = efs.load_efs(config)

        load_config_action = QAction('&Load settings', self)
        load_config_action.setShortcut('Ctrl+L')
        load_config_action.setStatusTip('Load settings')
        load_config_action.triggered.connect(self._show_load_config_dialog)

        # save_config_action = QAction('&Save settings', self)
        # save_config_action.setShortcut('Ctrl+S')
        # save_config_action.setStatusTip('Save settings')
        # save_config_action.triggered.connect(self.show_save_config_dialog)

        exit_action = QAction('Exit', self)
        exit_action.setShortcut('Ctrl+Q')
        exit_action.setStatusTip('Exit application')
        exit_action.triggered.connect(self.close)

        menubar = self.menuBar()
        file_menu = menubar.addMenu('&File')
        file_menu.addAction(load_config_action)
        # file_menu.addAction(save_config_action)
        file_menu.addAction(exit_action)

        self.start_button = QPushButton("S&tart MOO CEM")
        self.start_button.setDefault(True)
        self.start_button.setShortcut('Ctrl+T')
        self.start_button.clicked.connect(self._start_moocem)

        self.setCentralWidget(self.start_button)

        # self.statusBar()
        # self.setGeometry(300, 300, 350, 250)
        self.setWindowTitle('EFS3')

    def _show_load_config_dialog(self):
        """Shows the 'load config file' dialog."""
        options = QFileDialog.Options()
        config_path, _ = QFileDialog.getOpenFileName(self, "Load config", self.config.config_path,
                                                     "All Files (*);;JSON Files (*.json)", options=options)
        if config_path:
            self.config = EFSConfig(config_path)
            self.pem, self.moo = efs.load_efs(self.config)

    # def show_save_config_dialog(self):
    #     """Shows the 'save config file' dialog."""
    #     options = QFileDialog.Options()
    #     config_path, _ = QFileDialog.getOpenFileName(self, "Save config", self.config.config_path,
    #                                                  "All Files (*);;JSON Files (*.json)", options=options)
    #     if config_path:
    #         self.config.config_path = config_path
    #         self.config.save(config_path)

    def _start_moocem(self):
        """Starts the MOO computation and displays a progress dialog."""
        # NB: must only update GUI from main thread!
        self.start_button.setEnabled(False)
        progress_dialog = QProgressDialog(self)
        progress_dialog.setWindowTitle("MOO")
        progress_dialog.setMinimumWidth(300)
        progress_dialog.setLabelText("Computing")
        progress_dialog.setMinimumDuration(0)
        progress_dialog.setMaximum(self.config.moo_config.max_evals)
        # app.setOverrideCursor(QCursor(Qt.WaitCursor))

        time_estimator = TimeEstimator()
        time_estimator.set_maximum(self.config.moo_config.max_evals)
        canceled = threading.Event()
        job = {'success': False, 'elites': None, 'error': None}
        timer = QTimer()

        def cancel():
            canceled.set()
            self._signal.emit()

        def compute():
            def update_progress(i, n):
                time_estimator.make_step(i)
            try:
                job['elites'] = self.moo.run(update_progress, lambda: canceled.is_set())
                job['success'] = True
            except:
                import traceback
                job['error'] = traceback.format_exc()

            cancel()

        def update_progress_dialog():
            progress_dialog.setValue(time_estimator.pos)
            progress_dialog.setLabelText(time_estimator.format_time_remaining())
            progress_dialog.update()

        progress_dialog.canceled.connect(cancel)
        self._signal.connect(progress_dialog.close)

        # Updates dialog.
        timer.setInterval(200)
        timer.timeout.connect(update_progress_dialog)
        timer.start()

        # Performs the computation in a new worker thread.
        threading.Thread(target=compute, daemon=True).start()
        progress_dialog.exec()

        # Now the work is complete, or user opted to cancel.
        cancel()  # Ensures work is stopped if user canceled.
        timer.stop()
        self.start_button.setEnabled(True)

        if job['error'] is not None:
            dialog = QMessageBox()
            dialog.setWindowTitle("Error")
            dialog.setText("An error has occurred.\n\n" + job['error'])
            dialog.exec()
        elif job['success']:
            elites = job['elites']
            elite = self.moo.select_one_solution(elites)[np.newaxis, :]
            if self.config.select_single_solution and len(job['elites']):
                elites = elite

            results = efs.results_as_dataframe(elites, self.config)
            print(results)
            results.to_csv(self.config.output_path)

        dialog = QMessageBox()
        dialog.setText("Time elapsed: {0}.".format(time_estimator.format_elapsed_time()))
        if job['success']:
            dialog.setWindowTitle("Success")
        else:
            dialog.setWindowTitle("Failure")
        dialog.exec()


def run(config: EFSConfig):
    """Displays the EFS3 GUI."""
    app = QApplication(sys.argv)
    gui = GUI(config)
    gui.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    cfg = EFSConfig("config.json")
    cfg.setup_environment()
    run(cfg)
