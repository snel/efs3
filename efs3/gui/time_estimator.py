import time


class TimeEstimator(object):
    """Keeps track of the progress of a long-running task and estimates time remaining."""
    # This class borrows from `click._termui_impl`.

    def __init__(self, steps: int = 0):
        """
        Initializes a `TimeEstimator` object.

        Args:
            steps: The maximum number of steps the task will take.
        """
        super().__init__()

        self.avg = []
        self.start_time = self.last_update_time = time.time()
        self.end_time = None
        self.finished = False
        self.time_known = False
        self.length_known = steps is not None
        self.pos = 0
        self.length = 0

    def set_maximum(self, steps: int) -> None:
        """
        Sets the maximum number of steps the task will take.

        Args:
            steps: The maximum number of steps the task will take.
        """
        self.length = steps
        self.length_known = steps is not None

    def make_step(self, n_steps: int) -> None:
        """
        Advances the step counter by `n_steps`.

        Args:
            n_steps: Number of steps the task has completed.
        """
        self.pos += n_steps
        if self.length_known and self.pos >= self.length:
            self.finished = True

        if (time.time() - self.last_update_time) < 1.0:
            return

        self.last_update_time = time.time()
        self.avg = self.avg[-6:] + [-(self.start_time - time.time()) / self.pos]
        self.time_known = self.length_known

    def finish(self):
        """Marks the task as complete."""
        self.time_known = False
        self.finished = True

    @property
    def percent(self):
        """Returns the fraction the task has been completed in [0, 1]."""
        if self.finished:
            return 1.
        return min(self.pos / (float(self.length) or 1), 1.0)

    @property
    def time_per_step(self):
        """Returns the average time taken per step."""
        if not self.avg:
            return 0.
        return sum(self.avg) / float(len(self.avg))

    @property
    def time_remaining(self):
        """Returns the estimated time remaining for the task in seconds."""
        if self.length_known and not self.finished:
            return self.time_per_step * (self.length - self.pos) - time.time() + self.last_update_time
        return 0.

    @staticmethod
    def _format_time_delta(t):
        """Returns a string formatted with the time duration `t`."""
        seconds = t % 60
        t /= 60
        minutes = t % 60
        t /= 60
        hours = t % 24
        t /= 24
        if t > 0:
            days = t
            return '%dd %02d:%02d:%02d' % (days, hours, minutes, seconds)
        else:
            return '%02d:%02d:%02d' % (hours, minutes, seconds)

    def format_time_remaining(self):
        """Returns a string formatted with estimated time remaining."""
        if self.time_known:
            return self._format_time_delta(self.time_remaining + 1)
        return ''

    @property
    def elapsed_time(self):
        """Returns the time elapsed since starting the task. If the task has ended, this is the time until it ended."""
        if self.end_time is None:
            return time.time() - self.start_time
        else:
            return self.end_time - self.start_time

    def format_elapsed_time(self):
        """Returns a string formatted with the elapsed time."""
        return self._format_time_delta(self.elapsed_time)
