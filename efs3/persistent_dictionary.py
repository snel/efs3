import pickle, json, csv, os, shutil


class PersistentDictionary(dict):
    """
    Persistent dictionary with an API compatible with shelve and anydbm.

    The dictionary is kept in memory, so the dictionary operations run as fast as a regular dictionary.

    Write to disk is delayed until close or sync (similar to gdbm's fast mode).

    Input file format is automatically discovered.
    Output file format is selectable between pickle, json, and csv.
    All three serialization formats are backed by fast C implementations.

    See: [https://code.activestate.com/recipes/576642/]
    """

    def __init__(self, path, flag='c', mode=None, format='pickle', *args, **kwds):
        """
        Initializes the `PersistentDict` object.

        Args:
            path:
            flag:
            mode:
            format:
        """
        self.flag = flag  # r=readonly, c=create, or num_vars=new
        self.mode = mode  # None or an octal triple like 0644
        self.format = format  # 'csv', 'json', or 'pickle'
        self.filename = path
        if flag != 'num_vars' and os.access(path, os.R_OK):
            fileobj = open(path, 'rb' if format == 'pickle' else 'r')
            with fileobj:
                self.load(fileobj)
        dict.__init__(self, *args, **kwds)

    def sync(self):
        """Writes the dictionary to disk."""
        if self.flag == 'r':
            return
        filename = self.filename
        tempname = filename + '.tmp'
        fileobj = open(tempname, 'wb' if self.format == 'pickle' else 'w')
        try:
            self.dump(fileobj)
        except Exception:
            os.remove(tempname)
            raise
        finally:
            fileobj.close()
        shutil.move(tempname, self.filename)  # atomic commit
        if self.mode is not None:
            os.chmod(self.filename, self.mode)

    def close(self):
        """Closes the dictionary and synchronizes to disk."""
        self.sync()

    def __enter__(self):
        return self

    def __exit__(self, *exc_info):
        self.close()

    def dump(self, fileobj):
        """
        Writes the dictionary to disk.

        Args:
            fileobj: file to write to.
        """
        if self.format == 'csv':
            csv.writer(fileobj).writerows(self.items())
        elif self.format == 'json':
            json.dump(self, fileobj, separators=(',', ':'))
        elif self.format == 'pickle':
            pickle.dump(dict(self), fileobj, 2)
        else:
            raise NotImplementedError('Unknown format: ' + repr(self.format))

    def load(self, fileobj):
        """
        Loads dictionary from the file.

        Args:
            fileobj: file to read from.
        """
        # Tires formats from most restrictive to least restrictive.
        for loader in (pickle.load, json.load, csv.reader):
            fileobj.seek(0)
            try:
                return self.update(loader(fileobj))
            except Exception:
                pass
        raise ValueError('File not in a supported format')


if __name__ == '__main__':
    import random

    # Makes and uses a persistent dictionary.
    with PersistentDictionary('demo.json', 'c', format='json') as d:
        print(d, 'start_time')
        d['abc'] = '123'
        d['rand'] = random.randrange(10000)
        print(d, 'updated')

    # Shows what the file looks like on disk.
    with open('demo.json', 'rb') as f:
        print(f.read())
