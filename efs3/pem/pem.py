# Important NumPy notes:
# 1. If b is a boolean array, x[b] returns a new array, not a view into x.
#    This means that x[b].fill(0) does not modify x!
#    However, the syntax x[b] = c is different and sets the values in-place.
#    Therefore, use x[b] = 0 instead.
# 2. When using NumPy ufuncs with the `where` parameter, do not fail to specify an output buffer.
#    Otherwise, at indices that `where` is false, the results will be those from the first input vector,
#    and not simply zero!
# 3. Rather do computations in-place for all vector operations to avoid object allocations.

import numpy as np

from efs3.pem.pem_config import PEMConfig


class PEM(object):
    """
    Primary Energy module (PEM) of EFS.

    Simulates the actual electricity generation at each power station and the coal stockpile level
    at each coal-fired power station over the study period.

    Regarding precision:
       Results from this module may not correspond exactly to the original R implementation due to numerical
       inaccuracies being handled slightly differently. These differences may add up to hundreds of rands over a period
       of one year.

    Terminology:
       R: The R language.
       EFS2: The R implementation of EFS (Brits 2016).
       Stockpile day (SPD): On average, the quantity of coal required to fuel power station `p` for one day.
           Thus `m = sd` where m=mass in ktons, s=stockpile-days, d=days.
       Stockpile level: The quantity of coal on hand at power station `p`.

    Notation:
       In comments, identifiers marked with angle brackets "<>" denote corresponding variables in EFS2.
    """

    # Each day's computation is dependent on results from the previous day.
    # This makes it impossible to parallelize the daily loop.
    # However, each Monte Carlo loop is independent of the others, so it's possible to parallelize over them.

    class State(object):
        """
        Contains all transient state of a `PEM` instance; unique to each compute process.

        Instances of `PEM` don't store an instance of it as a member, but pass it between methods.
        This facilitates garbage collection and parallelization, although the class doesn't support parallel
        execution yet.
        """

        class _RunBuf(object):
            def __init__(self, config: PEMConfig):
                """
                Initializes the buffer.

                Args:
                    config: Configuration object.
                """
                m = config.monte_carlo_replications

                self.holding_cost = np.empty(m)
                """Holding cost for coal stocks, for each Monte Carlo iteration."""  # @EFS2: <repStock>

                self.mean_stock_outside_limits = np.empty(m)
                """Mean coal stock levels outside limits."""  # @EFS2: <mean_stock_outside_limits>

                self.transfer_cost = np.empty(m)
                """Total coal transfer cost, for each Monte Carlo iteration."""  # @EFS2: <TRANSFERCOST>

            def clear(self) -> None:
                """Clears the buffer."""

                self.holding_cost.fill(0)
                self.mean_stock_outside_limits.fill(0)
                self.transfer_cost.fill(0)

        class _RunMCBuf(object):
            def __init__(self, config: PEMConfig):
                """
                Initializes the buffer.

                Args:
                    config: Configuration object.
                """
                n = config.coal.num_stations

                self.prev_stock = np.empty(n)
                """Previous day's coal stocks (in ktons)."""  # @EFS2: <prevStock>

                self.stock = np.empty((n, config.num_days + 1))
                """Matrix of coal stockpile levels after each day (in SPD)."""  # @EFS2: <STOCK>
                # TODO: transpose me for possibly better performance.

                self.transfers = np.empty(n)
                """Coal transfers."""  # @EFS2: <Transfers>

                self.mat = np.empty((config.num_days + 1, n))
                """Temporary matrix."""

            def clear(self) -> None:
                """Clears the buffer."""

                self.prev_stock.fill(0)
                self.stock.fill(0)
                self.transfers.fill(0)
                self.mat.fill(0)

        class _RunDayBuf(object):
            def __init__(self, config: PEMConfig):
                """
                Initializes the buffer.

                Args:
                    config: Configuration object.
                """
                n = config.coal.num_stations

                self.stock = np.empty(n)
                """Stockpile levels at end of day (in ktons)."""  # @EFS2: <Coal 12, S>

                self.stock_spd = np.empty(n)
                """Stockpile levels at end of day (in SPD)."""  # @EFS2: <Coal 13, SPD>

                self.daily_transfer_cost = np.empty(n)
                """Daily coal transfer costs."""  # @EFS2: <TransferCost>

            def clear(self) -> None:
                """Clears the buffer."""
                self.stock.fill(0)
                self.stock_spd.fill(0)
                self.daily_transfer_cost.fill(0)

        class _StockpileLevelsBuf(object):
            """Buffer for `_stockpile_levels`."""

            # We could alias class's properties to store data to a single numpy array, as in the original
            # EFS2. This may improve cache locality in some cases. But keeping it this way should
            # reduce computations and memory use.

            def __init__(self, config: PEMConfig):
                """
                Initializes the buffer.

                Args:
                    config: Configuration object.
                """
                n = config.coal.num_stations

                # These columns appeared in @EFS2, but we don't use them.
                # """Actual coal deliveries by road (in ktons).""" # @EFS2: <Coal 4, AD_road>
                # """Actual coal deliveries by railway (in ktons).""" # @EFS2: <Coal 5, AD_rail>

                self.planned_load = np.empty(n)
                """Planned power load (in MWh)."""  # @EFS2: <Coal 2, PG>

                self.deliveries_conveyor = np.empty(n)
                """Actual coal deliveries by conveyor belt (in ktons).
                Also described as "actual deliveries for the day". """  # @EFS2: <Coal 3, AD_conveyor>

                self.actual_cv = np.empty(n)
                """Actual/Simulated calorific value of coal (in MJ/kg)."""  # @EFS2: <Coal 6, SimCV>

                self.burn_rate = np.empty(n)
                """Coal burn rate (in kg/MWh)."""  # @EFS2: <Coal 7, BurnRate>

                self.planned_eaf = np.empty(n)
                """Planned EAF (in percent)."""  # @EFS2: <Coal 8, PlannedEAF>

                self.actual_eaf = np.empty(n)
                """Actual EAF (in percent)."""  # @EFS2: <Coal 9, SimEAF>

                self.power_generated = np.empty(n)
                """Amount of power generated (in MWh)."""  # @EFS2: <Coal 10, AG>

                self.coal_burnt = np.empty(n)
                """Amount of coal burnt (in ktons)."""  # @EFS2: <Coal 11, AB>

                # Cached value
                self.planned_burn_rate = np.divide(config.coal.hr, config.coal.cv, where=config.coal.cv > 0,
                                                   out=np.zeros(config.coal.num_stations))
                self.planned_burn_rate *= 10. ** -6
                """Planned coal burn rates."""

            def clear(self) -> None:
                """Clears the buffer."""
                self.planned_load.fill(0)
                self.deliveries_conveyor.fill(0)
                self.actual_cv.fill(0)
                self.burn_rate.fill(0)
                self.planned_eaf.fill(0)
                self.actual_eaf.fill(0)
                self.power_generated.fill(0)
                self.coal_burnt.fill(0)

        class _CoalTransfersBuf(object):
            """Buffer for `_coal_transfers`."""

            def __init__(self, config: PEMConfig):
                """
                Initializes the buffer.

                Args:
                    config: Configuration object.
                """
                n = config.coal.num_stations

                # These columns appeared in EFS2, but we don't use them.
                # """Lower warning limit (in ktons).""" # @EFS2: <TransferMatrix1 2, LWL>
                # """Target stockpile level (in ktons). """ # @EFS2: <TransferMatrix1 3, TARGET>
                # """Upper warning limit (in ktons).""" # @EFS2: <TransferMatrix1 4, UWL>
                # """Stockpile after transfer (in ktons).""" # @EFS2: <TransferMatrix1 7, AT S(ktons)>
                # """Receiving capacity> (in ktons).""" # @EFS2: <TransferMatrix1 8>

                self.s = np.empty(n)
                """Initial coal stockpile (in ktons)."""  # @EFS2: <1, S(ktons)>

                self.bts = np.empty(n)
                """Stockpile before transfer (in ktons)."""  # @EFS2: <5, BT S(ktons)>

                self.transfer = np.empty(n)
                """Stockpile to transfer (in ktons)."""  # @EFS2: <6, Transfer>

            def clear(self) -> None:
                """Clears the buffer."""
                self.s.fill(0)
                self.bts.fill(0)
                self.transfer.fill(0)

        def __init__(self, config: PEMConfig):
            """
            Initializes internal state for a `PEM` object.

            Args:
                config: Configuration object.
            """
            n = config.coal.num_stations

            # *** Cached values ***
            # These remain constant after each reset.

            self.lwl = np.empty(n)
            """Lower warning limit (in ktons)."""

            self.target = np.empty(n)
            """Target warning level (in ktons)."""

            self.uwl = np.empty(n)
            """Upper warning limit (in ktons)."""

            self.initial_stock = np.empty(n)
            """Initial coal stockpile level (in ktons)."""  # @EFS2: <Coal 1, IS>

            self.initial_stock_spd = np.empty(n)
            """Initial stock levels (in SPD)."""  # @EFS2: <initialStock>

            # *** Method buffers ***

            self.run_buf = self._RunBuf(config)
            """Buffer for `_run`."""

            self.run_mc_buf = self._RunMCBuf(config)
            """Buffer for `_monte_carlo`, reset on every Monte Carlo iteration."""

            self.run_day_buf = self._RunDayBuf(config)
            """Buffer for `_monte_carlo`, reset on every daily iteration."""

            self.stockpile_levels_buf = self._StockpileLevelsBuf(config)
            """Buffer for `_stockpile_levels`."""  # @EFS2: <Coal>

            self.coal_transfers_buf = self._CoalTransfersBuf(config)
            """Buffer for `_coal_transfers`."""  # @EFS2: <TransferMatrix1/TransferTemp1>

        def reset(self, config: PEMConfig, w: np.ndarray) -> None:
            """
            Resets the coal stockpile and burn limits.

            Args:
                config: `PEMConfig` object.
                w: Decision variables -- a vector of `num_vars`*3 stock levels (in ktons).

                    Of the form `[LWL_0, TGT_0, UWL_0, ..., LWL_i, TGT_i, UWL_i, ...]` for `i` in `0, 1, ..., num_vars`
                    where
                        * `num_vars`: number of coal stations
                        * `LWL_i`: lower warning limit
                        * `TGT_i`: target amount above `LWL_i`
                        * `UWL_i`: upper warning limit above `TGT_i`
            """
            # @EFS2: w <check>
            # Note that we don't need to reset the buffers here. Users of each buffer take care of that themselves.
            n = config.coal.num_stations
            lower_warning_limit_sdb = np.zeros(n)
            target_stockpile_level_sdb = np.zeros(n)
            upper_warning_limit_sdb = np.zeros(n)

            for i in range(n):
                j = 3 * i
                lower_warning_limit_sdb[i] = w[j]
                target_stockpile_level_sdb[i] = w[j] + w[j + 1]
                upper_warning_limit_sdb[i] = w[j] + w[j + 1] + w[j + 2]

            np.copyto(self.initial_stock_spd, target_stockpile_level_sdb)
            np.copyto(self.initial_stock, target_stockpile_level_sdb)
            np.copyto(self.lwl, lower_warning_limit_sdb)
            np.copyto(self.target, target_stockpile_level_sdb)
            np.copyto(self.uwl, upper_warning_limit_sdb)

            self.initial_stock *= config.coal.sdb
            self.lwl *= config.coal.sdb
            self.target *= config.coal.sdb
            self.uwl *= config.coal.sdb

    def __init__(self, config: PEMConfig):
        """Initializes the `PEM` object."""
        self._config = config
        """"`PEMConfig` object."""

    def run(self, state: State, update_progress: callable = None, kill_switch: callable = None) -> None:
        """
        Runs the simulation for the configured number of Monte Carlo iterations.

        Args:
            state: `PrimaryEnergy.State` instance.
            update_progress: Callback function of the form `f(i, n)` that will be called whenever `i` out of `n`
                steps have been performed.
            kill_switch: A function returning a boolean value. If `True`, computations will stop as soon as possible.
        """
        config = self._config
        run_buf = state.run_buf
        run_buf.clear()

        # Monte Carlo loop.
        for mc in range(config.monte_carlo_replications):
            if kill_switch is not None and kill_switch():
                return

            def _update_progress(i, n):
                if update_progress is not None:
                    update_progress(i * mc, config.num_days * config.monte_carlo_replications)

            holding_cost, mean_stock_outside_limits, transfer_cost = self._monte_carlo(state,
                                                                                       _update_progress,
                                                                                       kill_switch)
            run_buf.holding_cost[mc] = holding_cost
            run_buf.mean_stock_outside_limits[mc] = mean_stock_outside_limits
            run_buf.transfer_cost[mc] = transfer_cost

    def _monte_carlo(self, state: State, update_progress: callable = None, kill_switch: callable = None) -> (
            float, float, float):
        """
        Runs the simulation once for the study period.

        Args:
            state: `PrimaryEnergy.State` instance.

        Returns:
            (holding cost, mean stock outside limits, transfer cost)
        """
        config = self._config
        mc_buf = state.run_mc_buf
        day_buf = state.run_day_buf

        mc_buf.clear()
        np.copyto(mc_buf.prev_stock, state.initial_stock)
        np.copyto(mc_buf.stock[:, 0], state.initial_stock_spd)
        transfer_cost = 0

        # Daily loop.
        for day in range(config.num_days):
            day_buf.clear()

            cost = self._sim_day(state, day, mc_buf.prev_stock, mc_buf.transfers,
                                 out_stock=day_buf.stock, out_transfers=mc_buf.transfers)

            if config.coal.transfers_allowed:
                transfer_cost += cost

            np.copyto(mc_buf.prev_stock, day_buf.stock)

            np.divide(day_buf.stock, config.coal.sdb, where=config.coal.sdb != 0,
                      out=day_buf.stock_spd)
            np.copyto(mc_buf.stock[:, day + 1], day_buf.stock_spd)

            if kill_switch is not None and kill_switch():
                return None, None, None
            if update_progress is not None:
                update_progress(day, config.num_days)

        # Computes mean stockpile level.
        # stock_burnt multiplies each column of `stock` (each day) element-wise with `sdb`.
        # The rows of stock_burnt are days, columns are stations.
        stock_burnt = mc_buf.stock.T * config.coal.sdb
        fsum = np.mean(stock_burnt, axis=0)

        # Computes holding cost.
        holding_cost = np.sum(fsum)

        # Computes stockpile levels below limits for each station.
        np.subtract(stock_burnt, state.lwl, where=stock_burnt < state.lwl, out=mc_buf.mat)
        stock_below = np.mean(mc_buf.mat, axis=0)

        # Computes stockpile levels over limits for each station.
        mc_buf.mat.fill(0)
        np.subtract(state.uwl, stock_burnt, where=stock_burnt > state.uwl, out=mc_buf.mat)
        stock_over = np.mean(mc_buf.mat, axis=0)

        # Computes mean stockpile level outside limits.
        mean_stock_outside_limits = (np.sum(stock_below) + np.sum(stock_over)) / self._config.num_days

        return holding_cost, mean_stock_outside_limits, transfer_cost

    def _sim_day(self, state: State, day: int, prev_stock: np.ndarray, transfers: np.ndarray,
                 out_stock: np.ndarray, out_transfers: np.ndarray) -> float:
        """
        Runs the simulation for a given day.

        Args:
            state: `PrimaryEnergy.State` instance.
            day: Number of days passed since start_time of study period.
            prev_stock: Previous day's stock levels.
            transfers: Stock transfers made so far.
            out_stock: Output storage for stockpile levels (in ktons).
            out_transfers: Output storage for transfers.
            
        Returns:
            Total cost of transfers for the day.
        """
        config = self._config

        # Computes coal stockpile levels.
        self._stockpile_levels(state, day, prev_stock, transfers,
                               out_stock=out_stock)

        # Computes coal transfers.
        if config.coal.transfers_allowed:
            cost = self._coal_transfers(state, day, out_stock,
                                        out_transfers=out_transfers)
        else:
            cost = 0.
        return cost

    def _stockpile_levels(self, state: State, day: int, prev_stock: np.ndarray, transfers: np.ndarray,
                          out_stock: np.ndarray) -> None:
        """
        Computes coal stockpile levels for a given day.

        Args:
            state: State object.
            day: Day for which to compute.
            prev_stock: Stockpile levels on previous day (in ktons).
            transfers: Stock transferred.
            out_stock: Output storage for stockpile levels (in ktons).
        """
        # @EFS2: prev_stock: <prev_stock>, out_stock: <Coal 12>
        config = self._config
        buf = state.stockpile_levels_buf
        buf.clear()

        # Planned load = (planned burn / (planned burn rate x 10^-6)) (MWh)
        np.divide(config.coal.burn_plan[:, day], buf.planned_burn_rate, where=buf.planned_burn_rate > 0,
                  out=buf.planned_load)

        # Actual daily delivery = planned delivery +/- uncertainty.
        np.copyto(buf.deliveries_conveyor,
                  (config.coal.delivery_plan[:, day] + config.coal.delivery_uncertainty(1)[:, 0]).clip(min=0))

        # Actual CV = planned CV +/- uncertainty.
        # Added non-negative clipping not present in R.
        np.copyto(buf.actual_cv,
                  (config.coal.cv + config.coal.cv_uncertainty(1)[:, 0]).clip(min=0))

        # Computes burn rate (heat rate / CV).
        np.divide(config.coal.hr, buf.actual_cv, where=buf.actual_cv > 0,
                  out=buf.burn_rate)

        # Planned EAF.
        np.copyto(buf.planned_eaf, config.coal.eaf[:, day])

        # Actual PCLF = planned PCLF +/- uncertainty.
        actual_pclf = (config.coal.pclf[:, day] + config.coal.pclf_uncertainty(1)[:, 0]).clip(min=0)

        # Actual UCLF = planned UCLF +/- uncertainty.
        actual_uclf = (config.coal.uclf[:, day] + config.coal.uclf_uncertainty(1)[:, 0]).clip(min=0)

        # Actual EAF = 100 - actual PCLF - actual UCLF - planned OCLF.
        buf.actual_eaf = (-actual_pclf - actual_uclf - config.coal.oclf[:, day] + 100.).clip(min=0)

        # Actual power generated = planned load * EAF ratio.
        np.copyto(buf.power_generated, buf.actual_eaf)
        buf.power_generated /= np.maximum(buf.planned_eaf, 1.)  # EAF ratio
        buf.power_generated *= buf.planned_load

        # Doesn't seem important, but included this as EFS2 did so.
        np.around(buf.planned_eaf, decimals=5,
                  out=buf.planned_eaf)

        # Computes amount of coal burnt.
        np.copyto(buf.coal_burnt, buf.power_generated)
        buf.coal_burnt *= buf.burn_rate
        buf.coal_burnt *= 10. ** -6

        # Computes stockpile level at the end of the day.
        np.copyto(out_stock, prev_stock)
        out_stock += buf.deliveries_conveyor
        out_stock -= buf.coal_burnt
        out_stock += transfers

        # Finally, we ensure that stockpile levels never fall below zero.
        # Selects stations with negative stockpile levels.
        x = out_stock < 0
        # Resets their stockpile levels to zero.
        out_stock[x] = 0  # Not fill(0), see comment at top of module.
        # Sets amount burnt to the amount on stockpile left at end of previous day.
        np.copyto(buf.coal_burnt, prev_stock, where=x)

        # Updates the amount of power generated.
        y = np.divide(buf.coal_burnt, buf.burn_rate, where=buf.burn_rate > 0,
                      out=np.zeros(config.coal.num_stations))
        y *= 10. ** 6
        np.copyto(buf.power_generated, y, where=x)
        return

    def _coal_transfers(self, state: State, day: int, stock: np.ndarray,
                        out_transfers: np.ndarray) -> float:
        """
        Computes coal transfers for a given day, assuming they are allowed.

        It is assumed that previous days have been computed, so that the results from the previous day are reflected in
        the `stock` parameter.

        Args:
            state: `State` object.
            day: Day for which to compute.
            stock: Stockpile levels (in ktons).
            out_transfers: Output storage for transfers.

        Returns:
            Total cost of transfers.
        """
        # @EFS2: stock: <Coal 12>, out_transfers: <Transfers>
        config = self._config
        buf = state.coal_transfers_buf
        buf.clear()
        total_cost = 0.

        if day % config.stockpile_evaluation_constraint != 0:
            out_transfers.fill(0)
        else:
            np.copyto(buf.s, stock)
            np.copyto(buf.bts, stock)

            # *** Receiving ***

            # Determines the order in which stations must receive.
            # Note: equal rows (e.g. zeros) may be sorted differently than in EFS2.
            np.copyto(buf.transfer,
                      state.lwl - buf.bts, where=buf.bts < state.lwl)
            # Indices with column 5 in reverse order.
            if self._config.mimic_efs2:
                order = (-buf.transfer).argsort(kind='mergesort')
            else:
                order = buf.transfer.argsort()[::-1]

            buf.transfer.fill(0)  # Clears transfer buffer.

            buf.s = buf.s[order]
            buf.bts = buf.bts[order]

            for i in range(len(order)):
                if buf.bts[i] < state.lwl[order[i]]:
                    cost = self._transfer(state, buf, i, False, order,
                                          out_transfers=out_transfers)
                    total_cost += cost
                else:
                    if i == 0:
                        out_transfers.fill(0)
                    break  # All required stocks have been received.

        # *** Giving ***

        # Determines the order in which stations must serve.
        np.copyto(buf.transfer,
                  buf.bts - state.uwl, where=buf.bts < state.uwl)
        # Indices with column 5 in reverse order.
        if self._config.mimic_efs2:
            order = (-buf.transfer).argsort(kind='mergesort')
        else:
            order = buf.transfer.argsort()[::-1]

        buf.transfer.fill(0)  # Clears transfer buffer.

        buf.s = buf.s[order]
        buf.bts = buf.bts[order]

        for i in range(len(order)):
            if buf.bts[i] > state.uwl[order[i]]:
                cost = self._transfer(state, buf, i, True, order,
                                      out_transfers=out_transfers)
                total_cost += cost
            else:
                break  # All excess stocks have been served.

        return total_cost

    # noinspection PyProtectedMember
    def _transfer(self, state: State, buf: State._CoalTransfersBuf, station_index: int, give_not_receive: bool,
                  order: np.ndarray,
                  out_transfers: np.ndarray) -> float:
        """
        Computes coal transfers.

        Args:
            buf: Buffer, modified in-place.
            station_index: Index of current station (in transfer order).
            give_not_receive: If true, the station will serve. Otherwise it will receive.
            order: Order of transfers.
            out_transfers: Output storage for stock transferred (in ktons).

        Returns:
            Total cost of transfers.
        """
        # @EFS2: out_transfers: <Transfers>
        # TODO: There is too much sorting going on here. We might be able to simplify this.
        config = self._config
        pos_trans = config.coal.possible_transfers[order, order[station_index]]
        distances = config.coal.transfer_distances[order, order[station_index]]
        target = state.target[order]

        # r: Amount needed to reach target.
        if give_not_receive:
            r = target[station_index] - buf.bts[station_index]
            test = np.subtract(target * pos_trans, buf.bts * pos_trans,
                               where=(buf.bts * pos_trans) < (target * pos_trans),
                               out=np.zeros(config.coal.num_stations))
        else:
            r = buf.bts[station_index] - target[station_index]
            test = np.subtract(buf.bts * pos_trans, target * pos_trans,
                               where=(buf.bts * pos_trans) > (target * pos_trans),
                               out=np.zeros(config.coal.num_stations))

        # In R, comparison with 0 is less precise. In Python, comparison with 0 yield more positives.
        test_positive = test > 1e-3  # 0.
        # Subset of non-zero indices.
        nonzero_indices = np.flatnonzero(test_positive)

        if not nonzero_indices.size:
            cost = 0.
        else:
            distances_subset = distances[nonzero_indices]
            # Indices into subset, sorted by distance in ascending order.
            if self._config.mimic_efs2:
                by_distance = distances_subset.argsort(kind='mergesort')
            else:
                by_distance = distances_subset.argsort()

            # Results are station indices, in visiting order.
            if self._config.coal.transfer_policy == 1:
                indices = nonzero_indices[by_distance]
            else:
                indices = nonzero_indices

            for i in indices:
                transfer_cap = config.coal.transfer_caps[order[i]]
                transfer_amount = min(np.abs(r), test[i], transfer_cap)
                if give_not_receive:
                    buf.transfer[station_index] = -transfer_amount
                    buf.transfer[i] = transfer_amount
                else:
                    buf.transfer[station_index] = transfer_amount
                    buf.transfer[i] = -transfer_amount

                # TODO: It's not really necessary to do vector addition;
                # we could do it for the appropriate indices instead.
                buf.bts += buf.transfer
                buf.transfer.fill(0)

                r = buf.bts[station_index] - target[station_index]

            x2 = nonzero_indices[by_distance]
            cost_v = distances_subset * np.abs(buf.bts[x2] - buf.s[x2])
            # TODO Can use dot product instead, should be slightly faster.
            cost = np.sum(cost_v)

        # Since the indices were permuted, we need to take the inverse 
        # permutation to get them back in the original order.
        p_inv = np.zeros((config.coal.num_stations, config.coal.num_stations), dtype=int)
        for i in range(config.coal.num_stations):
            p_inv[order[i], i] = 1
        inverse_order = np.nonzero(p_inv)[1]

        transfers = buf.bts - buf.s
        np.copyto(out_transfers, transfers[inverse_order])
        return cost

    def _objectives2(self, f1, f2, f3) -> list:
        """
        Returns a vector of 2 objectives according to model type. Inputs are 0-arg functions.

        Args:
            f1: Computes first objective: holding cost.
            f2: Computes second objective: coal transfer cost.
            f3: Computes third objective: stock outside limits.

        Returns:
            A 2-element list.
        """
        if self._config.model_type == 1:
            if not self._config.coal.transfers_allowed:
                return [f1(), None]
            else:
                return [f1(), f2()]
        else:
            return [f1(), f3()]

    def objectives(self, state: State) -> list:
        """Returns two objectives for the MOO CEM algorithm."""

        def percentile(x, p):
            return x.sort()[np.round(p * self._config.monte_carlo_replications)]

        if self._config.output_statistic == 1:  # Expected value.
            return self._objectives2(
                lambda: np.mean(state.run_buf.holding_cost),
                lambda: np.mean(state.run_buf.transfer_cost),
                lambda: np.mean(state.run_buf.mean_stock_outside_limits)
            )
        elif self._config.output_statistic == 2:  # 20th percentile value
            return self._objectives2(
                lambda: percentile(state.run_buf.holding_cost, .2),
                lambda: percentile(state.run_buf.transfer_cost, .2),
                lambda: percentile(state.run_buf.mean_stock_outside_limits, .2)
            )
        elif self._config.output_statistic == 3:  # 80th percentile value
            return self._objectives2(
                lambda: percentile(state.run_buf.holding_cost, .8),
                lambda: percentile(state.run_buf.transfer_cost, .8),
                lambda: percentile(state.run_buf.mean_stock_outside_limits, .8)
            )

    def _wm_as_df(self, state: State, order=None):
        """Debugging method. Returns the coal "working matrix" as a `DataFrame`."""
        n = self._config.coal.num_stations
        m = state.stockpile_levels_buf
        q = state.run_day_buf

        cols = np.array(
            [state.initial_stock, m.planned_load, m.deliveries_conveyor, np.zeros(n), np.zeros(n),
             m.actual_cv, m.burn_rate, m.planned_eaf, m.actual_eaf, m.power_generated, m.coal_burnt, q.stock,
             q.stock_spd]).T
        col_names = np.array(
            ["IS (ktons)", "PG (MWh)", "AD_conveyor (ktons)", "AD_road (ktons)",
             "AD_rail (ktons)", "SimCV (MJ/kg)", "BurnRate (kg/MWh)", "PlannedEAF (%)",
             "SimEAF (%)", "AG (MWh)", "AB (ktons)", "S (ktons)", "S (SPD)"])
        row_names = self._config.coal.stations

        if order is not None:
            cols = cols[order]
            row_names = row_names[order]

        import pandas as pd
        x = pd.DataFrame(cols, index=row_names, columns=col_names)
        return x

    def _t_as_df(self, state: State, order=None):
        """Debugging method. Returns the "transfers matrix" as a `DataFrame`."""
        m = state.coal_transfers_buf
        cols = np.array(
            [m.s, state.lwl, state.target, state.uwl, m.bts, m.transfer, np.zeros(self._config.coal.num_stations),
             self._config.coal.transfer_caps]).T
        col_names = np.array(
            ["S(ktons)", "LWL", "TARGET", "UWL", "BT S(ktons)", "Transfer", "AT S(ktons)", "Receiving capacity"])
        row_names = self._config.coal.stations

        if order is not None:
            row_names = row_names[order]
            cols[:, 0] = cols[:, 0][order]
            cols[:, 1] = cols[:, 1][order]
            cols[:, 2] = cols[:, 2][order]
            cols[:, 3] = cols[:, 3][order]
            cols[:, 7] = cols[:, 7][order]

        import pandas as pd
        x = pd.DataFrame(cols, index=row_names, columns=col_names)
        return x
