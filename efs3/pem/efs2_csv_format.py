"""
Provides access to `PEM` configuration in the CSV format used by EFS2.
"""
import warnings
import numpy as np
import pandas as pd

from efs3.pem.pem_config import PEMConfig, no_uncertainty
from efs3.rstats import rnorm, rlogis, rtriangle, rweibull3, rlnorm3, rt, runif, rBeta_ab


# For:
#   `_read__coal_station_series`, `_read__station_series`,
# These types of files would be better transposed, then we can read them more efficiently.

def _read__coal_station_vector(config: PEMConfig, path: str) -> pd.DataFrame:
    n = config.coal.num_stations
    return pd.read_csv(path, header=None, nrows=n, usecols=range(3)).set_index([1])[2].squeeze()


def _read__coal_station_matrix(config: PEMConfig, path: str):
    n = config.coal.num_stations
    return pd.read_csv(path, header=0, index_col=0, usecols=range(1, n + 2), nrows=n + 1)


def _read__coal_station_series(config: PEMConfig, path: str) -> pd.DataFrame:
    n = config.coal.num_stations
    data = pd.read_csv(path, header=None, nrows=n + 1).iloc[:, 1:].T
    data.set_index([0], inplace=True)
    data.rename(columns=data.iloc[0, :], inplace=True)
    data.drop(data.index[0], inplace=True)
    return data


def _read__station_series(config: PEMConfig, path: str) -> pd.DataFrame:
    n = config.other.num_stations + config.coal.num_stations
    data = pd.read_csv(path, header=None, nrows=n + 1).iloc[:, 1:].T
    data.set_index([0], inplace=True)
    data.rename(columns=data.iloc[0, :], inplace=True)
    data.drop(data.index[0], inplace=True)
    return data


def _str_to_bool(s):
    """
    Returns the boolean value encoded in the string.

    Args:
        s: Boolean encoded in string.
    """
    if s in ('True', '1'):
        return True
    elif s in ('False', '0'):
        return False
    else:
        return None


def _split_station_series(data: pd.DataFrame) -> (pd.DataFrame, pd.DataFrame):
    """
    Divides the input data in two.
    The PCLF, UCLF, ... files contain the coal stations at an arbitrary position.

    Returns:
        Two matrices, one for coal stations and one for the others.
    """
    # TODO Currently assumes stations are the rows, dates are columns.
    # If they are transposed, we'll want to retranspose, do this, and then transpose back.
    coal = data.iloc[3:19, :]
    other = data.drop(data.index[3:19])
    return coal, other


def _read_settings_data(config, path: str) -> None:
    data = pd.read_csv(path, header=None, usecols=range(2)).set_index([0]).squeeze()

    start_date = pd.to_datetime(data["Start date"])
    end_date = pd.to_datetime(data["End date"])
    config.num_days = (end_date - start_date).days + 1

    config.coal.transfers_allowed = _str_to_bool(data["Coal transfers allowed"])
    if config.coal.transfers_allowed is None:
        raise ValueError("Coal transfers allowed must be either True or False.")

    config.coal.transfer_policy = int(data["Coal transfer policy"])
    if config.coal.transfer_policy not in (1, 2):
        raise ValueError("Coal transfer policy must be either 1 (closest first) or 2 (most urgent).")

    config.model_type = int(data["Model 1 or Model 2"])
    if config.model_type not in (1, 2):
        raise ValueError("Model must be either 1 or 2.")

    config.monte_carlo_replications = int(data["Monte Carlo replications"])
    # if config.monte_carlo_replications < 10:
    #    raise ValueError("Monte Carlo replications must be at least 10.")
    if config.monte_carlo_replications < 10:
        warnings.warn(
            "PrimaryEnergy output statistics only valid for >= 10 Monte Carlo replications.")

    config.output_statistic = int(data["Output statistic"])
    if config.output_statistic not in (1, 2, 3):
        raise ValueError("Output statistic must be either 1, 2 or 3.")

    config.stockpile_evaluation_constraint = int(data["Stockpile evaluation constraint"])
    if config.stockpile_evaluation_constraint < 0:
        raise ValueError("Stockpile evaluation constraint must be non-negative.")

    config.coal.transfer_caps = data[8:24].values.astype(float)
    if (config.coal.transfer_caps < 0).any():
        raise ValueError("Coal transfer caps must be non-negative.")


def _read_cv_data(config, path: str) -> None:
    data = _read__coal_station_vector(config, path)
    config.coal.cv = data.values.astype(np.float)


def _read_hr_data(config, path: str) -> None:
    data = _read__coal_station_vector(config, path)
    config.coal.hr = data.values.astype(np.float) * 1000.


def _read_sdb_data(config, path: str) -> None:
    data = _read__coal_station_vector(config, path)
    # TODO: we want to make the algorithm use the untransposed version instead.
    config.coal.sdb = data.T.values.astype(np.float)


def _read_burn_plan_data(config, path: str) -> None:
    data = _read__coal_station_series(config, path)
    # TODO: we want to make the algorithm use the untransposed version instead.
    config.coal.burn_plan = data.T.values.astype(np.float) / 1000.


def _read_delivery_plan_data(config, path: str) -> None:
    data = _read__coal_station_series(config, path)
    # TODO: we want to make the algorithm use the untransposed version instead.
    config.coal.delivery_plan = data.T.values.astype(np.float) / 1000.


def _read_pclf_data(config, path: str) -> None:
    data = _read__station_series(config, path)
    # TODO: we want to make the algorithm use the untransposed version instead.
    (coal_pclf, other_pclf) = _split_station_series(data.T)
    config.coal.pclf = coal_pclf.values.astype(np.float)
    config.other.pclf = other_pclf.values.astype(np.float)


def _read_uclf_data(config, path: str) -> None:
    data = _read__station_series(config, path)
    # TODO: we want to make the algorithm use the untransposed version instead.
    (coal_uclf, other_uclf) = _split_station_series(data.T)
    config.coal.uclf = coal_uclf.values.astype(np.float)
    config.other.uclf = other_uclf.values.astype(np.float)


def _read_oclf_data(config, path: str) -> None:
    data = _read__station_series(config, path)
    # TODO: we want to make the algorithm use the untransposed version instead.
    (coal_oclf, other_oclf) = _split_station_series(data.T)
    config.coal.oclf = coal_oclf.values.astype(np.float)
    config.other.oclf = other_oclf.values.astype(np.float)


def _read_possible_transfers_data(config, path: str) -> None:
    data = _read__coal_station_matrix(config, path)
    config.coal.possible_transfers = data.values.astype(np.float)
    # The current matrix is not symmetric at all.
    # if (data.T != data).any().any():
    #  raise ValueError("Possible transfers matrix must be symmetric.")


def _read_transfer_distances_data(config, path: str) -> None:
    data = _read__coal_station_matrix(config, path)
    data = data.values.astype(np.float)
    config.coal.transfer_distances = data
    if (data.T != data).any().any():
        raise ValueError("Transfer distances matrix must be symmetric.")


def set_hardcoded_uncertainty_distributions(config: PEMConfig) -> None:
    """
    This saves uncertainty distributions to the config object.

    These were hard-coded in the R implementation of EFS.
    TODO We should save/read these to/from a file instead.
    """
    # Note that sampling should be much faster if we specify n>1 ahead of time,
    # instead of computing them each time.

    config.coal.delivery_uncertainty = lambda n: np.array([
        rnorm(n, -1.6, 13.8),
        np.zeros(n),
        rBeta_ab(n, shape1=12359, shape2=25.6, a=-30399, b=61.3),
        rlogis(n, location=-3.24, scale=4.68),
        rlogis(n, location=-0.19, scale=2.73),
        rlogis(n, location=-1.32, scale=6.56),
        rlogis(n, location=1.76, scale=2.98),
        rBeta_ab(n, shape1=2774, shape2=65.6, a=-1813.6, b=41.56),
        np.zeros(n),
        rBeta_ab(n, shape1=866.1, shape2=115.4, a=-848.1, b=114.46),
        rlogis(n, location=-0.07, scale=5.83),
        rlogis(n, location=0.56, scale=5.76),
        rlogis(n, location=-1.66, scale=2.98),
        rBeta_ab(n, shape1=51.6, shape2=164.4, a=-49.1, b=150.7),
        rBeta_ab(n, shape1=33.1, shape2=84.2, a=-29.25, b=70.36),
        rBeta_ab(n, shape1=377.5, shape2=316.8, a=-150.8, b=124.9)
    ])

    config.coal.cv_uncertainty = lambda n: np.array([
        rtriangle(n, -0.23, 2.6, 1.7),
        np.zeros(n),
        rtriangle(n, -1.28, 0.75, 0.27),
        rlogis(n, location=0.24, scale=0.24),
        runif(n, -0.17, 1.13),
        rlogis(n, location=-0.06, scale=0.45),
        rtriangle(n, -1.77, 1.06, -0.38),
        rnorm(n, 2.07, 0.52),
        np.zeros(n),
        rBeta_ab(n, shape1=1.67, shape2=3.41, a=-0.56, b=2.66),
        rBeta_ab(n, shape1=1179.3, shape2=19.96, a=-158.1, b=3.07),
        rtriangle(n, -4.16, 3.4, -3.12),
        rtriangle(n, 0.37, 2.84, 0.99),
        runif(n, -1.94, 1.1),
        rlogis(n, 0.3, 0.35),
        rBeta_ab(n, 19.74, 4.23, -7.1, 1.5)
    ])

    # Stations:
    #     Matimba, Medupi, Lethabo, Duvha, Hendrina, Matla, Kriel1_3, Kriel4_6,
    #     Kusile, Kendal, Majuba, Tutuka, Camden, Arnot, Komati, Grootvlei

    config.coal.pclf_uncertainty = lambda n: np.array([
        rnorm(n, -0.47, 9.35),
        np.zeros(n),  # XXX: Distribution unknown.
        rBeta_ab(n, 13, 4.97, -98.53, 36.54),
        rBeta_ab(n, 6.38, 22.29, -44.8, 146.38),
        rlogis(n, -2.06, 6.5),
        rnorm(n, -0.46, 14.9),
        rBeta_ab(n, 15.94, 906.7, -60.1, 3732.4),
        rtriangle(n, -55.7, 54.25, -3.44),
        np.zeros(n),  # XXX: Distribution unknown.
        rnorm(n, -2.31, 8.06),
        rlogis(n, 3.46, 5.27),
        rnorm(n, 0.34, 11.9),
        rBeta_ab(n, 7.85, 12.9, -31.46, 60.83),
        runif(n, -20.9, 27.6),
        rlogis(n, -0.98, 5.09),
        rlogis(n, -7.3, 7.33)
    ])

    # We don't use `config.other.pclf_uncertainty` and `config.other.uclf_uncertainty` at this time.
    # Stations:
    #     Acacia, Ankerlig, Gourikwa, PortRex, Gariep, Vanderkloof, Drakensberg, Ingula, Palmiet, Koeberg.
    # These should really have been ordered like this:
    #     CahoraBassa, IPPs, Koeberg, Ankerlig, Gourikwa, Acacia, PortRex, Unmet, Gariep, Vanderkloof,
    #     Drakensberg, Palmiet, Ingula.
    config.other.pclf_uncertainty = lambda n: np.array([
        rweibull3(n, 1.86E08, 1.33E09, -1.33E09),
        rBeta_ab(n, 1003.4, 50.82, -561.62, 26.6),
        rBeta_ab(n, 1.27E06, 14, -2.07E06, 19.84),
        rweibull3(n, 5.02, 46.98, -43.5),
        rt(n, 2),
        rt(n, 2),
        rBeta_ab(n, 99337, 7.98, -5.1E05, 36.9),
        np.zeros(n),  # XXX: Distribution unknown.
        rt(n, 2),
        rt(n, 2)
    ])

    config.coal.uclf_uncertainty = lambda n: np.array([
        rBeta_ab(n, 4.62, 1.23E07, -10.34, 3.75E07),
        np.zeros(n),  # XXX: Distribution unknown.
        rlogis(n, 3.36, 3.41),
        rnorm(n, 15.57, 10.33),
        rweibull3(n, 1.61, 33.2, -22.84),
        rweibull3(n, 2.53, 18.62, -15),
        rweibull3(n, 4.37, 54.3, -44.97),
        rnorm(n, 5.32, 16.1),
        np.zeros(n),  # XXX: Distribution unknown.
        rBeta_ab(n, 2.18, 5.54, -8.2, 37.7),
        rBeta_ab(n, 3578.6, 1903.2, -1088.3, 581.24),
        rBeta_ab(n, 0.74, 1.2, -7.88, 27.7),
        rnorm(n, 2.4, 7),
        rtriangle(n, -13.2, 24.8, 13.5),
        rweibull3(n, 2.4, 35.24, -19.12),
        rtriangle(n, -14.7, 34.7, 1.24)
    ])

    config.other.uclf_uncertainty = lambda n: np.array([
        rlogis(n, 0.14, 0.27),
        rBeta_ab(n, 3.6, 1.5E07, -0.78, 4.4E06),
        rlogis(n, 0.11, 0.36),
        rlnorm3(n, 0.39, 0.55, -1.18),
        rweibull3(n, 6.71E07, 2.08E07, -2.08E07),
        rlnorm3(n, 1.05, 0.16, -2.9),
        rweibull3(n, 0.8, 7.39, -2.76),
        np.zeros(n),  # XXX: Distribution unknown.
        rBeta_ab(n, 16, 1.77E07, -7.7, 8.9E06),
        rlnorm3(n, 2.49, 0.5, -12.9)
    ])


def read_config(
                settings_path: str,
                burn_plan_path: str,
                delivery_plan_path: str,
                transfer_distances_path: str,
                cv_path: str,
                oclf_path: str,
                pclf_path: str,
                sdb_path: str,
                uclf_path: str,
                hr_path: str,
                possible_transfers_path: str,
                config
                ) -> PEMConfig:
    """
    Loads the `PEMConfig` with data read from the given paths (in EFS2 CSV file format).

    Uncertainty distributions will be set to 0.

    Args:
        settings_path: Path to "settings" CSV file.
        burn_plan_path: Path to "burn plan" CSV file.
        delivery_plan_path: Path to "delivery plan" CSV file.
        transfer_distances_path: Path to "transfer distances" CSV file.
        cv_path: Path to "CV" CSV file.
        oclf_path: Path to "OCLF" CSV file.
        pclf_path: Path to "PCLF" CSV file.
        sdb_path: Path to "SDB" CSV file.
        uclf_path: Path to "UCLF" CSV file.
        hr_path: Path to "heat rates" CSV file.
        possible_transfers_path: Path to "possible transfers" CSV file.
        config: `PEMConfig` object.

    Returns:
        Loaded `EFSConfig`.
    """

    for f, path in [
        ["settings", settings_path],
        ["burn_plan", burn_plan_path],
        ["delivery_plan", delivery_plan_path],
        ["transfer_distances", transfer_distances_path],
        ["cv", cv_path],
        ["oclf", oclf_path],
        ["pclf", pclf_path],
        ["sdb", sdb_path],
        ["uclf", uclf_path],
        ["hr", hr_path],
        ["possible_transfers", possible_transfers_path]
    ]:
        # Gets function to read CSV file.
        read = globals()["_read_" + f + "_data"]
        # Reads data into the config object.
        read(config, path)

    config.coal.eaf = -config.coal.pclf - config.coal.uclf - config.coal.oclf + 100.
    # TODO: We should probably enforce that this be non-zero. See `pem._stockpile_levels()`

    return config
