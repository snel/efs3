import logging
import numpy as np


class PEMConfig(object):
    """Contains all fixed input parameters to a `PEM` instance."""

    class GenConfig(object):
        """Generic config regarding stockpiles and power stations."""

        def __init__(self):
            """Initializes the `PEMConfig` object."""
            self.stations = []
            """List of power stations."""

            self.num_stations = 0
            """Number of power stations."""

            self.burn_plan = np.empty(0)
            """Burn plan."""  # @EFS2: <PBurn>

            self.pclf = np.empty(0)
            """Planned capability loss factor (planned maintenance)."""  # @EFS2: <PlannedPCLF>

            self.uclf = np.empty(0)
            """Unplanned capability loss factor (unplanned maintenance)."""  # @EFS2: <PlannedUCLF>

            self.oclf = np.empty(0)
            """Other capability loss factor (other unplanned outages)."""  # @EFS2: <PlannedOCLF>

            self.eaf = np.empty(0)
            """Energy availability factor.
             Note: this is computed in pem_config.read_config()."""  # @EFS2: <PlannedEAF>

            self.delivery_plan = np.empty(0)
            """Planned coal delivery (ktons)."""  # @EFS2: <PlannedDelivery>

    class OtherConfig(GenConfig):
        """Config regarding non-coal-fired power stations."""

        def __init__(self):
            """Innitializes the `OtherConfig` object."""
            self.pclf_uncertainty = lambda n: np.empty(0)
            """
            Uncertainty distribution for PCLF's.

            Args:
                n: Number of samples to return.

            Returns:
                (m, n) `ndarray`, where `m` is the number of stations and `n` is number of samples.
            """

            self.uclf_uncertainty = lambda n: np.empty(0)
            """
            Uncertainty distribution for UCLF's.

            Args:
                n: Number of samples to return.

            Returns:
                (m, n) `ndarray`, where `m` is the number of stations and `n` is number of samples.
            """

            super().__init__()

    class CoalConfig(GenConfig):
        """Config regarding coal stockpiles and coal-fired power stations."""

        def __init__(self):
            """Innitializes the `CoalConfig` object."""
            self.transfers_allowed = False
            """Coal transfers allowed (True or False)."""  # @EFS2: <CT>

            self.transfer_policy = 1
            """Coal transfer policy (1: closest first, 2: most urgent)."""  # @EFS2: <policy>

            self.transfer_caps = np.empty(0)
            """Coal transfer caps."""  # @EFS2: <caps>

            self.cv = np.empty(0)
            """Coal calorific values (CV's) (MJ/kg)."""  # @EFS2: <AllCV>

            self.hr = np.empty(0)
            """Heat rates (MJ/kWh) - assuming each station's HR remains constant throughout."""  # @EFS2: <AllHR>

            self.sdb = np.empty(0)
            """Standard daily burn.  """  # @EFS2: <SDB>

            self.possible_transfers = np.empty(0)
            """Possible coal transfers matrix."""  # @EFS2: <PossibleTransfers>

            self.transfer_distances = np.empty(0)
            """Coal transfer distance matrix."""  # @EFS2: <DistanceMatrix>

            self.delivery_uncertainty = lambda n: np.empty(0)
            """
            Uncertainty distribution for deliveries.

            Args:
                n: Number of samples to return.

            Returns:
                (m, n) `ndarray`, where `m` is the number of stations and `n` is number of samples.
            """

            self.cv_uncertainty = lambda n: np.empty(0)
            """
            Uncertainty distribution for CV's.

            Args:
                n: Number of samples to return.

            Returns:
                (m, n) `ndarray`, where `m` is the number of stations and `n` is number of samples.
            """

            self.pclf_uncertainty = lambda n: np.empty(0)
            """
            Uncertainty distribution for PCLF's.

            Args:
                n: Number of samples to return.

            Returns:
                (m, n) `ndarray`, where `m` is the number of stations and `n` is number of samples.
            """

            self.uclf_uncertainty = lambda n: np.empty(0)
            """
            Uncertainty distribution for UCLF's.

            Args:
                n: Number of samples to return.

            Returns:
                (m, n) `ndarray`, where `m` is the number of stations and `n` is number of samples.
            """

            super().__init__()

    def __init__(self, coal: CoalConfig = CoalConfig(), other: OtherConfig = OtherConfig(),
                 num_days: int = 0, model_type: int = 1, monte_carlo_replications: int = 0,
                 output_statistic: int = 1, stockpile_evaluation_constraint: int = 0,
                 mimic_efs2: bool = False, log: logging.Logger = logging.root):
        """
        Initializes the `PEMConfig` object.

        Args:
            coal: Configuration regarding coal stockpiles and coal power stations.
            other: Configuration regarding non-coal power stations.
            num_days: Number of days in study period.
            model_type: An integer denoting Model 1 or Model 2.
            monte_carlo_replications: Number of Monte Carlo replications.
            output_statistic: An integer denoting which statistic to return from the generation function.
                May be one of the following:
                1: mean,
                2: 20th percentile value,
                3: 80th percentile value.
            stockpile_evaluation_constraint: Stockpile evaluation constraint, that is, the number of days between
                evaluations.
            mimic_efs2: When set, `PEM` will attempt to produce results as close as possible to EFS2, but computation
                speed might be reduced.
            log: Information log.
        """
        # @EFS2: output_statistic: <statistic>
        self.coal = coal
        """Config regarding coal stockpiles and coal power stations."""

        self.other = other
        """Config regarding non-coal power stations."""

        self.num_days = num_days
        """Number of days in study period."""  # @EFS2: <num_days>

        self.model_type = model_type
        """An integer denoting Model 1 or Model 2."""  # @EFS2: <Model>:

        self.monte_carlo_replications = monte_carlo_replications
        """Number of Monte Carlo replications."""  # @EFS2: <MCrep>

        self.output_statistic = output_statistic
        """An integer denoting which statistic to return from the generation function.
        May be one of the following:
        1: mean,
        2: 20th percentile value,
        3: 80th percentile value."""  # @EFS2: <statistic>

        self.stockpile_evaluation_constraint = stockpile_evaluation_constraint
        """Stockpile evaluation constraint, that is, the number of days between evaluations."""  # @EFS2: <evaluate>

        self.mimic_efs2 = mimic_efs2
        """When set, `PEM` will attempt to produce results as close as possible to EFS2, but computation
        speed might be reduced.
        For example, R uses a stable sorting algorithm whereas Python does not, by default."""

        self.log = log
        """Information log."""


def no_uncertainty(config):
    """Resets the randomness distributions to return 0."""

    def zero(n):
        return np.zeros((config.coal.num_stations, n))

    config.coal.delivery_uncertainty = zero
    config.coal.cv_uncertainty = zero
    config.coal.pclf_uncertainty = zero
    config.other.pclf_uncertainty = zero
    config.coal.uclf_uncertainty = zero
    config.other.uclf_uncertainty = zero
