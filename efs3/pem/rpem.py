"""A Python wrapper to `PEM.R`, from the R implementation of EFS2."""

import os
import numpy as np
from efs3.pem.pem_config import PEMConfig


class PEM(object):
    """
    A Python wrapper to `PEM.R`, from the R implementation of EFS2.
    This wrapper provides the interface demanded of `PEM` objects.
    """

    class State(object):
        def __init__(self, config: PEMConfig, update_progress: callable=None, kill_switch: callable=None):
            self.w = None
            """w: Decision variables -- a vector of `num_vars`*3 stock levels (in ktons)."""

            self.update_progress = update_progress
            """Callback function `f(i,n)` to measure computation progress, where `i` is the number of steps completed
            and `n` is the number of steps to complete."""

            self.kill_switch = kill_switch
            """A function returning a boolean value. If `True`, computations will stop as soon as possible."""

        def reset(self, config: PEMConfig, w: np.ndarray):
            self.w = w

    def __init__(self, config: PEMConfig):
        self._config = config

        # Loads R code.
        from rpy2.robjects.packages import SignatureTranslatedAnonymousPackage
        with open(self._config.r_pem_path) as f:
            text = f.read()
            self._r_pem = SignatureTranslatedAnonymousPackage(text, "r_pem")

    def run(self, state) -> None:
        # Gets an R instance.
        import rpy2.robjects as robjects

        # Sets the R working directory (required for relative paths). This also changes the Python working directory.
        wd = os.getcwd()
        robjects.r('setwd("{0}")'.format(self._config.efs_r_path))

        # Runs R code.
        output = self._r_pem.Generation(state.w)

        # Resets the Python working directory.
        os.chdir(wd)

        # Most reliable solution for converting from R to NumPy,
        # see: http://stackoverflow.com/questions/31271181/
        # how-to-convert-an-r-complex-matrix-into-a-numpy-array-using-rpy2
        output = np.array(list(output))[:, 0]
        state.output = output

    def objectives(self, state: State) -> list:
        return state.output