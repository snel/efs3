import unittest
import numpy as np
import copy

from efs3.pem.pem import PEM
from efs3.pem.pem_config import PEMConfig, no_uncertainty
from test.test_config import test_config

ones = np.ones(48)
""" Bogus input vector of ones (in stockpile-days).
 For all stations, lower limit = upper limit = target = 1. """

lower = np.array(
    [10, 1, 1,
     0, 0, 0,
     10, 1, 1,
     10, 1, 1,
     10, 1, 1,
     10, 1, 1,
     10, 1, 1,
     10, 1, 1,
     0, 0, 0,
     10, 1, 1,
     10, 1, 1,
     10, 1, 1,
     10, 1, 1,
     10, 1, 1,
     10, 1, 1,
     10, 1, 1])
""" Minimum values of decision variables (in stockpile-days). """

upper = np.array(
    [25, 15, 15,
     0, 0, 0,
     25, 15, 15,
     25, 15, 15,
     25, 15, 15,
     25, 15, 15,
     25, 15, 15,
     25, 15, 15,
     0, 0, 0,
     25, 15, 15,
     25, 15, 15,
     25, 15, 15,
     25, 15, 15,
     25, 15, 15,
     25, 15, 15,
     25, 15, 15])
""" Maximum values of decision variables (in stockpile-days). """

w1 = np.array(
    [1.8905527, 0.8281214, -0.3735315,
     0.0000000, 0.0000000, 0.0000000,
     -1.1752986, -0.9953136, 0.6699908,
     0.5876804, -0.6090802, 0.2451434,
     -0.4291862, 0.7131400, -0.6047332,
     0.4964038, -0.3351462, -1.4067368,
     0.4611583, 0.4769873, -0.7072183,
     -1.7162665, -0.6122357, 1.0036072,
     0.0000000, 0.0000000, 0.0000000,
     -0.7894984, -0.9158537, 2.7061767,
     0.5410319, -0.7809867, 0.9123852,
     1.0472354, -1.9094285, 0.7842724,
     0.3925207, 0.3119377, -0.4643572,
     -0.2298117, -0.7939667, 0.2990415,
     -1.7728453, -2.3193526, -1.0563946,
     1.4698669, -0.7446063, 1.1825158])


def generation(pem: PEM, w: np.ndarray):
    state = PEM.State(pem._config)
    state.reset(pem._config, w)
    pem.run(state)
    return pem.objectives(state)


class PrimaryEnergyTests(unittest.TestCase):
    """ Test suite for the Primary Energy module.

    Most of these tests rely on comparing to known results from the R implementation. Since we need to test against
    these fixed values, these tests are not flexible to changes in the number of decision variables. This implies that
    if any coal stations are added or removed, these tests will be invalidated.
    """

    # *** Helper methods ***

    def _gen_test(self, config: PEMConfig, tests, rtol=1.e-5, atol=1.e-8):
        """
        Tests the generation function against known results with transfers disallowed.

        Config:
            Uncertainties: all set to 0.
            Transfers: disallowed.

        Args:
            tests: Matrix of inputs and expected outputs. First column are inputs, second column are outputs.
            rtol: Relative error tolerance.
            atol: Absolute error tolerance.
        """

        config = copy.copy(config)
        no_uncertainty(config)
        # No uncertainty, so replications shouldn't make a difference.
        config.monte_carlo_replications = 1

        for (w, output0) in tests:
            pem = PEM(config)
            output = generation(pem, w)
            output = [x if x is not None else 0 for x in output]
            # print("a) {0}\nb) {1}\num_vars\num_vars".format(output0, output))
            self.assertTrue(
                np.allclose(output, output0, rtol=rtol, atol=atol),
                "Input: {0}.\nExpected output: {1}\nActual output: {2}.".format(w, output0, output))

    def _gen_rep_test(self, config: PEMConfig):
        """
        Tests the generation function works the same over multiple replications, barring random variables.

        Config:
            Uncertainties: all set to 0.

        Selects a random vector and computes the generation function output.
        The outputs are verified to be the same for multiple Monte Carlo replications.

        Repeats the test for a few random input vectors.
        """

        config = copy.copy(config)
        no_uncertainty(config)

        for _ in range(2):
            # Selects a random positive input vector of size 50 == (config.coal.num_stations * 3 + 2).
            w = np.random.normal(0, 1000, 50)

            # Computes the result for one replication.
            config.monte_carlo_replications = 1
            pem = PEM(config)
            output0 = generation(pem, w)
            output0 = [x if x is not None else 0 for x in output0]
            # Compares against multiple replications.
            for mc in [1, 10]:
                config.monte_carlo_replications = mc

                pem = PEM(config)
                output = generation(pem, w)
                output = [x if x is not None else 0 for x in output]
                self.assertTrue(
                    np.allclose(output, output0),
                    "Unexpected output: {0}.\nExpected: {1}.\nmc={2}.".format(output, output0, mc))

    # *** Test cases ***

    def test_gen_replications(self):
        """
        Tests the generation function works the same over multiple replications, barring random variables.

        Config:
            Uncertainties: all set to 0.
        """

        config = test_config().pem_config
        config.coal.transfers_allowed = False
        self._gen_rep_test(config)

        config.coal.transfers_allowed = True
        self._gen_rep_test(config)

    def test_gen_no_transfers_1(self):
        """
        Tests the generation function against known results from @EFS2.

        Config:
            Transfers: disallowed
            Uncertainties: all set to 0
            Number of days: 1--4, 14
        """

        config = test_config().pem_config
        config.coal.transfers_allowed = False
        config.monte_carlo_replications = 1
        config.ignored = [1, 8]

        config.num_days = 1
        tests = [
            [ones, [830.2307, 0.0]],
            [lower, [4507.146, 0.0]],
            [upper, [16354.98, 0.0]],
            [w1, [-2.997559, 0.0]]
        ]
        self._gen_test(config, tests)

        config.num_days = 2
        tests = [
            [ones, [843.3691, 0.0]],
            [lower, [4520.284, 0.0]],
            [upper, [16368.12, 0.0]],
            [w1, [61.06379, 0.0]]
        ]
        self._gen_test(config, tests)

        config.num_days = 3
        tests = [
            [ones, [856.5075, 0.0]],
            [lower, [4533.423, 0.0]],
            [upper, [16381.26, 0.0]],
            [w1, [99.82001, 0.0]]
        ]
        self._gen_test(config, tests)

        config.num_days = 4
        tests = [
            [ones, [869.6459, 0.0]]
        ]
        self._gen_test(config, tests)

        # 14 passes. 15 fails without error tolerance adjustment.
        config.num_days = 14

        tests = [
            [ones, [1001.03, 0.0]]
        ]
        self._gen_test(config, tests)

    def test_gen_with_transfers_1(self):
        """
        Tests the generation function against known results from @EFS2.

        Config:
            Transfers: allowed
            Uncertainties: all set to 0
            Number of days: 1--3
        """

        config = test_config().pem_config
        config.coal.transfers_allowed = True
        config.monte_carlo_replications = 1
        config.ignored = [1, 8]

        # Note: transfers don't seem to take place with the input vector of ones.

        config.num_days = 1
        tests = [
            [ones, [830.2307, 0.]],
            [lower, [4507.146, 0.]],
            [upper, [16354.98, 0.]]
        ]
        self._gen_test(config, tests, rtol=1.e-1)
        tests = [
            [w1, [-2.997556, 16025.53]]
        ]
        self._gen_test(config, tests, rtol=1.e2)

        config.num_days = 2
        tests = [
            [ones, [843.3691, 0.]],
            [lower, [4520.284, 0.]],
            [upper, [16368.12, 0.]]
        ]
        self._gen_test(config, tests, rtol=1.e-1)
        tests = [
            [w1, [76.01649, 23330.9]]
        ]
        self._gen_test(config, tests, rtol=1.e2)

        config.num_days = 3
        tests = [
            [ones, [856.5075, 0.]],
            [lower, [4533.423, 0.]],
            [upper, [16368.12, 0.]]
        ]
        self._gen_test(config, tests, rtol=1.e-1)
        tests = [
            [w1, [128.4796, 28482.17]]
        ]
        self._gen_test(config, tests, rtol=1.e2)

    def test_gen_no_transfers_15(self):
        """
        Tests the generation function against known results from @EFS2.

        Config:
            Transfers: disallowed
            Uncertainties: all set to 0
            Number of days: 55, 30, 40

        These tests fail within standard Numpy tolerance.
        This is presumably due to the build-up of numerical error being different between Python and R.
        Python output is 1014.1685163250002, R output is 1014.201.
        """

        config = test_config().pem_config
        config.coal.transfers_allowed = False
        config.monte_carlo_replications = 1
        config.ignored = [1, 8]

        config.num_days = 15
        tests = [
            [ones, [1014.201, 0.0]]
        ]
        self._gen_test(config, tests, rtol=1.e-3)

        config.num_days = 30
        tests = [
            [ones, [1219.154, 0.0]]
        ]
        self._gen_test(config, tests, rtol=1.e-2)

        config.num_days = 40
        tests = [
            [ones, [1336.634, 0.0]]
        ]
        self._gen_test(config, tests, rtol=1.e-1)

        # def test_gen365(self):
        #     """Tests the generation function against known results with transfers disallowed.
        #
        #     Config:
        #         Transfers: disallowed
        #         Uncertainties: all set to 0
        #         Number of days: 365
        #
        #         Fails due to numerical divergence.
        #     """
        #
        #     config = base_config()
        #     config.coal.transfers_allowed = False
        #     config.num_days = 365
        #
        #     tests = [
        #         [ones, [3837.958, 0.0]]
        #     ]
        #     self._gen_test(config, tests, rtol=1.e-1)


if __name__ == '__main__':
    unittest.main()
