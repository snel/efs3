from timeit import timeit
import numpy as np

from efs3.pem import pem_config
from efs3.pem.pem import PEM
from efs3.pem.pem_config import no_uncertainty


def _run(number=None) -> list:
    """
    Runs the generation module with arbitrary input.

    Args:
         number: Number of times to re-run the generation module.
    """

    config = pem_config.read_config("H:\Sean\Documents\Career\EFS\efs2\Inputs")
    no_uncertainty(config)
    pem = PEM(config)

    w = np.array(
        [1.8905527, 0.8281214, -0.3735315,
         0.0000000, 0.0000000, 0.0000000,
         -1.1752986, -0.9953136, 0.6699908,
         0.5876804, -0.6090802, 0.2451434,
         -0.4291862, 0.7131400, -0.6047332,
         0.4964038, -0.3351462, -1.4067368,
         0.4611583, 0.4769873, -0.7072183,
         -1.7162665, -0.6122357, 1.0036072,
         0.0000000, 0.0000000, 0.0000000,
         -0.7894984, -0.9158537, 2.7061767,
         0.5410319, -0.7809867, 0.9123852,
         1.0472354, -1.9094285, 0.7842724,
         0.3925207, 0.3119377, -0.4643572,
         -0.2298117, -0.7939667, 0.2990415,
         -1.7728453, -2.3193526, -1.0563946,
         1.4698669, -0.7446063, 1.1825158])

    if number is None:
        x = pem.generation(w)
    else:
        x = None
        state = pem.State(config)
        for _ in range(number):
            state.reset(config, w)
            pem.run(state)
            x = pem.objectives(state)

    return x


if __name__ == '__main__':
    def test():
        _run(number=80)


    np.set_printoptions(precision=1, suppress=True, linewidth=130)
    # np.random.seed(0)

    # Times execution of `num_vars` simulation steps of the PEM,
    # each with 10 Monte Carlo repetitions over the study period.
    t = timeit(test, number=1)
    print("Time: {0}".format(t))
