import unittest

from efs3.test.pem import pem, pem_config

if __name__ == '__main__':
    suites = [pem.PrimaryEnergyTests]
    for c in suites:
        suite = unittest.TestLoader().loadTestsFromTestCase(c)
        unittest.TextTestRunner(verbosity=2).run(suite)
