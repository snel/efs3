from efs3.efs_config import EFSConfig


def test_config() -> EFSConfig:
    """Returns test configuration."""
    return EFSConfig("test/test_config.json")
