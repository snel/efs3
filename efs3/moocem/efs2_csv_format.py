"""
Provides access to `MOOCEM` configuration in the CSV format used by EFS2.
"""
import pandas as pd
import numpy as np


def read_dv(n: int, dv_path: str) -> (np.ndarray, np.ndarray):
    """
    Reads decision variable ranges from a CSV file.

    Args:
        n: Number of stations.
        dv_path: Path to CSV file.

    Returns:
        Tuple of lower- and upper range vectors.
    """
    data = pd.read_csv(dv_path, header=None, skiprows=2, nrows=2, usecols=range(n * 3 + 1), index_col=0)
    return data.values[0, :], data.values[1, :]
