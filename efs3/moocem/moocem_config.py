import logging

import multiprocessing
import numpy as np


class MOOCEMConfig(object):
    """Configuration for a `MOOCEM` object."""

    def __init__(self, config_path: str, lower_limits: np.ndarray, upper_limits: np.ndarray, num_objectives: int = 2,
                 max_evals: int = 250, max_loops=20, alpha: float = .7, population_size: int = 20, p_h: float = .3,
                 epsilon: float = .1, ignored=None, mock_stats: bool = False, num_processes: int = None,
                 log: logging.Logger = logging.root):
        """
        Initializes configuration for a `MOOCEM` object.

        Args:
            config_path: Path to EFS config file. Used to create new processes.
            lower_limits: Lower limits of decision variables.
                (Between 1 and 20 stockpile days for each variable for all the coal-fired stations.)
            upper_limits: Upper limits of decision variables.
                (Between 1 and 20 stockpile days for each variable for all the coal-fired stations.)
            num_objectives: Number of objectives returned by PEM.
            max_evals: Maximum number of evaluations.
            max_loops: Maximum number of outer loops supporting the multi-objective search.
                In single-objective search, it would be one loop only.
            alpha: Alpha of the CEM (smoothing parameter). Set at 0.7--0.8.
            population_size: Population size.
            p_h: Probability to invert histograms to diversify search and to avoid trapping.
            epsilon: Epsilon of the CEM (common termination threshold). Set at 0.1--1.
            ignored: List of stations to ignore, e.g. `[1, 8]`. Their decision variables will always be set to 0.
            mock_stats: Enables mocking of statistical methods, to produce predictable results.
            num_processes: Number of processes to use for parallelisation. Must be larger than 0.
                Processes compute evaluation functions of individuals in parallel.
                Therefore, no more than `population_size` processes will be run at a time.
                If set to `None`, the number of CPU cores available will be used.
            log: Information logger.
        """
        # In @Brits the values were: alpha=.7, population_size=100, p_h=.3, epsilon=.1, max_evals=10000.

        self.config_path = config_path

        self.max_evals = max_evals
        """Maximum evaluations."""

        self.max_loops = max_loops
        """Maximum number of outer loops supporting the multi-objective search.
        In single-objective search, it would be one loop only."""

        self.lower_limits = lower_limits
        """Lower limits of decision variables."""

        self.upper_limits = upper_limits
        """Upper limits of decision variables."""

        self.num_vars = len(lower_limits)  # <- Must be the same as len(upper_limits), i.e. 42.
        """Number of decision variables."""  # @EFS2: <num_vars>, @Brits: <D>

        self.num_objectives = num_objectives
        """Number of objective functions, also called `K` (@Brits)."""  # @EFS2: <num_objectives>, @Brits: <K>

        self.alpha = alpha
        """Alpha of the CEM (smoothing parameter). Set at 0.7--0.8."""

        self.population_size = population_size
        """Population size."""

        self.p_h = p_h
        """Probability to invert histograms to diversify search and to avoid trapping."""

        self.epsilon = epsilon
        """Epsilon of the CEM (common termination threshold). Set at 0.1--1."""

        if ignored is None:
            ignored = []
        self.ignored = ignored
        """List of stations to ignore. Their decision variables will always be set to 0."""

        self.mock_stats = mock_stats
        """Samples random-values from a predictable sequence, for direct comparison to @EFS2."""

        self.log = log
        """Information log."""

        if num_processes is None:
            num_processes = multiprocessing.cpu_count() or 1
        self.num_processes = num_processes
        """Number of processes to use during evaluation. By default, it uses the number of CPU cores available."""
