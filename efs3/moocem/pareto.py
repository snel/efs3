import numpy as np


def pareto_rank(w: np.ndarray, d: int, k: int, p_e: float):
    """
    Pareto ranking (minimisation).

    Args:
        d: Number of decision variables.
        k: Number of objectives.
        w: (num_vars, d+k+1) working matrix.
        p_e: Threshold value.

    Returns:
        Rows in `w` with rank value not exceeding `p_e` as the weakly or non-dominated "elite" vector.
    """
    # 1. In @EFS2 the threshold was p_e+1, but neither his paper, nor @Bekker added the 1.
    # 2. Note that for loops are faster than while loops in Python.
    # 3. @Brits and @EFS2 added 1 to `j` and matrix indices.
    # 4. Sorting along the last axis is faster and uses less space than sorting along any other axis.
    #    See: [https://docs.scipy.org/doc/numpy/reference/generated/numpy.sort.html#numpy.sort]
    #    But sorting seems to have less impact on performance on MOO than array concatenation etc., since transposing
    #    the appropriate arrays appeared to degrade performance.

    n = w.shape[0]
    w[:, d + k] = 0.  # Clears rank values.

    for j in range(d, d + k - 2):
        # Sorts `w` with the values in column `j` in descending order.
        w = w[w[:, j]].argsort()[::-1]

        for r_p in range(n):
            for r_q in range(r_p, n):
                if w[r_p, d + k] >= p_e+1:  # XXX See comment #1.
                    break
                if w[r_p, j] >= w[r_q, j]:
                    w[r_p, d + k] += 1.  # Increments rank value.

    return w[w[:, d + k] <= p_e]
