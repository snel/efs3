import itertools
import time
from multiprocessing.pool import Pool

import multiprocessing
import numpy as np

from efs3.moocem.pareto import pareto_rank
from efs3.moocem.moocem_config import MOOCEMConfig
from efs3.pem.pem import PEM


class MOOCEM(object):
    """Multi-Objective Optimizer (MOO) for the EFS Primary Energy module using Cross-Entropy Method (CEM)."""

    class State(object):
        """Contains all transient state of a `MOOCEM` instance."""

        class _RunBuf(object):
            """Buffer for `run`."""

            def __init__(self, config: MOOCEMConfig):
                """
                Initializes the buffer.

                Args:
                    config: Configuration object.
                """
                n = config.num_vars
                m = config.num_objectives

                self.num_evals = 0
                """Number of evaluations performed."""

                self.mu = np.empty((0, n))
                """Means vector of the CEM."""

                self.sigma = np.empty((0, n))
                """Standard deviation vector of the CEM."""

                self.elite = np.empty((0, n + m + 1))
                """Elite vector preserving good solutions and driving the CEM."""

                self.sigma_eps = np.empty(n)
                """Keeps track of consecutive sigma differences for convergence checking."""

                self.lower_limits = np.empty(n)
                """Varying lower limit vector in histogram assignments.
                Initially set to `settings.lower_limits` (limits of decision variables)."""

                self.upper_limits = np.empty(n)
                """Varying upper limit vector in histogram assignments.
                Initially set to `settings.upper_limits` (limits of decision variables)."""

                # self.set_mu = np.empty((0, num_vars))
                # """Variable to collect each Elite mean after an iteration.
                # For info purposes, e.g. in thesis."""
                #
                # self.set_sigma = np.empty((0, num_vars))
                # """Variable to collect each Elite std. dev. after an iteration.
                # For info purposes, e.g. in thesis."""

                # We use a varying number `k+2` elements for any one loop,
                #   but we allocate enough storage to accomodate all possible growth.
                r_max = min(config.max_loops, config.max_evals) + 2

                self.bin_freq = np.empty(r_max)  # Integers, but `int` dtype is too small.
                """Histogram frequencies."""

                self.bin_edges = np.empty(r_max + 1)
                """Histogram bucket edges."""

                self.working_matrix = np.empty((config.population_size, config.num_vars + config.num_objectives + 1))
                """
                Matrix `X` of variable vectors `X_i` for i in [0,D) (Brits, 2016).
                The work area for the algorithm, including population of decision values,
                objective values and ranking.
                """

            def clear(self) -> None:
                """Clears the buffer."""
                self.num_evals = 0
                self.mu.fill(0)
                self.sigma.fill(0)
                self.elite.fill(0)
                self.sigma_eps.fill(0)
                self.lower_limits.fill(0)
                self.upper_limits.fill(0)
                # self.set_mu.fill(0)
                # self.set_sigma.fill(0)
                self.bin_freq.fill(0)
                self.bin_edges.fill(0)
                self.working_matrix.fill(0)

        def __init__(self, config: MOOCEMConfig):
            """Initializes internal state for a `MOOCEM` object."""
            self.run_buf = self._RunBuf(config)
            """Buffer for `_run`."""

    def __init__(self, config: MOOCEMConfig, pem: PEM):
        """
        Initializes a `MOOCEM` object.

        Args:
            config: Configuration object.
            pem: Primary energy module.
        """
        self._config = config
        """Configuration object."""

        self._pem = pem
        """Primary energy module."""

    def run(self, update_progress=None, kill_switch=None) -> np.ndarray:
        """
        Runs the optimizer and returns the elite set found.

        Args:
            update_progress: Callback function of the form `f(i, n)` that will be called whenever `i` out of `n`
                steps have been performed.
            kill_switch: A 0-arg function returning a boolean value.
                If `True`, computations will stop as soon as possible.

        Returns:
            A tuple of
            1. a single pareto-optimal solution selected (nearest to the mean), and
            2. n `(x, num_vars+num_objectives)` ndarray consisting of `x` pareto-optimal solutions.
        """

        config = self._config
        if config.mock_stats:
            from efs3.mock_stats import rtruncnorm, runif
        else:
            from efs3.rstats import rtruncnorm, runif

        start_time = time.time()
        config.log.info("MOO CEM started.")

        # Initializes buffers.
        pool = Pool(config.num_processes)
        pem_state = self._pem.State(self._pem._config)
        pem_state.kill_switch = kill_switch
        state = self.State(config)
        buf = state.run_buf
        buf.clear()

        # Defines some variable aliases.
        num_vars = config.num_vars
        num_objectives = config.num_objectives
        population_size = config.population_size

        for k in range(config.max_loops):
            # Initializes the CE vectors.
            buf.sigma_eps.fill(np.infty)
            np.copyto(buf.lower_limits, config.lower_limits)
            np.copyto(buf.upper_limits, config.upper_limits)
            # CEM requires an arbitrarily large initial sigma.
            # NB: sigma must be set to a float vector, or we'll encounter integer overflow.
            buf.sigma = 10. * (config.upper_limits[0:config.num_vars] - config.lower_limits[0:config.num_vars])
            # Selects a random mu value for each DV in its definition range.
            buf.mu = runif(num_vars, config.lower_limits[0:config.num_vars], config.upper_limits[0:config.num_vars])

            # We clear the `working_matrix` here like @EFS2 does, @Bekker does that inside the inner loop.
            buf.working_matrix.fill(0)

            # Inner loop will be governed by convergence.
            t = 0
            looping = True
            while looping:
                t += 1
                if not len(buf.elite[:, 0]) or k == 0:
                    # Builds initial population. Selects random values from the definition ranges of the DV's.
                    for i in range(num_vars):
                        buf.working_matrix[:, i] = rtruncnorm(population_size,
                                                              a=config.lower_limits[i], b=config.upper_limits[i],
                                                              mean=buf.mu[i], sd=buf.sigma[i])
                        # @EFS2 stops there, but then @Bekker did something like this:
                        # from scipy.stats import invgauss
                        # working_matrix[:, i] = invgauss.rvs(buf.mu[i], scale=buf.sigma[i], size=population_size)
                    # NOTE @EFS2 rounded off here--this directly affects whether we get integral results at the end.
                    np.round(buf.working_matrix[:, 0:num_vars], 0, out=buf.working_matrix[:, 0:num_vars])
                    # Note that we don't clip for positive numbers. This may help explore the search space better
                    #   and shouldn't be a problem later, since the target values are all positive, so results should
                    #   converge to positive values too.
                else:
                    r = k + 2  # @EFS2 had this set at r=k.
                    for i in range(num_vars):
                        buf.bin_edges.fill(0)
                        buf.bin_edges[0] = buf.lower_limits[i]
                        buf.bin_edges[r] = buf.upper_limits[i]
                        buf.bin_edges[1:r] = np.linspace(np.min(buf.elite[:, i]), np.max(buf.elite[:, i]),
                                                         num=r - 1)

                        # Fills the bin_freq array--a histogram with bins endpoints from `bin_edges`.
                        # A lower-level (compiled) version of this is likely to be much faster.
                        buf.bin_freq.fill(0)
                        # `itertools.product` should be faster than a nested loop as `r` and `elite` grows.
                        for w, h in itertools.product(range(r), range(len(buf.elite[:, i]))):
                            if buf.bin_edges[w] <= buf.elite[h, i] < buf.bin_edges[w + 1]:
                                buf.bin_freq[w] += 1

                        # Takes inverse of `bin_freq`, with probability `p_h`.
                        if runif(1, 0, 1) < config.p_h:
                            buf.bin_freq[0:(r + 1)] = np.max(buf.bin_freq) - buf.bin_freq[0:(r + 1)]

                        # Calculates ratios out of `population_size` in each bin.
                        # Must avoid underflow and division by zero.
                        s = np.sum(buf.bin_freq)
                        if s == 0:
                            buf.bin_freq.fill(0)
                        else:
                            np.floor_divide(population_size * buf.bin_freq, s, out=buf.bin_freq)

                        buf.bin_freq[r - 1] += population_size - s

                        # Indices into `working_matrix`.
                        up_to = 0
                        # Samples on each histogram bin range,
                        #   with the number of observations proportional to the histogram count.
                        for a in range(r):
                            if buf.bin_freq[a] == 0:
                                continue  # @EFS2: Apparent optimization not present in @Bekker.

                            start = up_to
                            up_to += int(buf.bin_freq[a])
                            buf.lower_limits[i] = buf.bin_edges[a]
                            buf.upper_limits[i] = buf.bin_edges[a + 1]

                            if start <= up_to:
                                # This is not the same mu as the initial mu; it moves with the histogram range.
                                temp_mu = runif(1, buf.lower_limits[i], buf.upper_limits[i])
                                temp_s = buf.upper_limits[i] - buf.lower_limits[i]

                                # Fills the working matrix.
                                # @Bekker did something different with Gaussian inverse distributions.
                                for s in range(start, up_to):
                                    # @EFS Added these if/else conditions to cover apparent corner-cases.
                                    if buf.upper_limits[i] > buf.lower_limits[i]:
                                        buf.working_matrix[s, i] = rtruncnorm(1,
                                                                              a=buf.lower_limits[i],
                                                                              b=buf.upper_limits[i],
                                                                              mean=temp_mu, sd=temp_s)
                                    else:
                                        buf.working_matrix[s, i] = buf.lower_limits[i]

                                    if np.floor(buf.working_matrix[s, i]) >= buf.lower_limits[i]:
                                        buf.working_matrix[s, i] = np.floor(buf.working_matrix[s, i])
                                    else:
                                        buf.working_matrix[s, i] = np.round(buf.working_matrix[s, i], 0)

                # Evaluates the `population_size` objective functions and stores results in the `working_matrix`.
                # Operates from parallel processes.
                obj = []
                for i in range(population_size):
                    w = buf.working_matrix[i, 0:config.num_vars]
                    objectives = pool.apply_async(self._evaluate, (config.config_path, w))
                    obj.append(objectives)
                for i, objectives in enumerate(obj):
                    if kill_switch is not None and kill_switch():
                        config.log.info("MOO CEM aborted. Time elapsed: {0}.".format(time.time() - start_time))
                        pool.close()
                        pool.join()
                        return buf.elite
                    buf.working_matrix[i, config.num_vars:(config.num_vars + config.num_objectives)] = objectives.get()
                    if update_progress is not None:
                        update_progress(1, self._config.max_evals)

                # Ranks the solutions and extracts any "good" solutions.
                temp_elite = pareto_rank(buf.working_matrix, num_vars, num_objectives, 2)
                # Adds `temp_elite` to form updated `elite` vector.
                buf.elite = np.vstack((buf.elite, temp_elite))

                # If good solutions were identified, `elite` can be exploited.
                if len(buf.elite[:, 0]) > 1:
                    all_eps = 0  # See @Bekker (missing from @EFS2).
                    for i in range(num_vars):
                        # Adjusts the mu's according to the CEM.
                        buf.mu[i] = ((1. - config.alpha) * buf.mu[i]
                                     + config.alpha * np.mean(buf.elite[:, i]))
                        buf.sigma_eps[i] = buf.sigma[i]

                        # Adjusts the sigma's according to the CEM.
                        # Numpy calculates std. dev. with population_size as denominator,
                        #   while R uses `population_size`-1. We set them to give the same results as @EFS2.
                        # Also, to prevent integer overflow, we must cast `sigma` to float.
                        buf.sigma[i] = ((1. - config.alpha) * buf.sigma[i]
                                        + config.alpha * np.std(buf.elite[:, i], ddof=1))

                        # Calculates the consecutive changes, to determine convergence.
                        buf.sigma_eps[i] = np.abs(buf.sigma[i] - buf.sigma_eps[i])

                        # If this i-th decision variable has converged, mark it.
                        all_eps += buf.sigma_eps[i] > config.epsilon

                    # If all variables converged, terminates inner loop.
                    if all_eps == 0:
                        looping = False
                    pass
                    # Records the current mu and sigma for academic information (i.e. not usually required.)
                    # buf.set_mu = np.vstack((buf.set_mu, buf.mu))
                    # buf.set_sigma = np.vstack((buf.set_sigma, buf.sigma))

                buf.num_evals += population_size
                if buf.num_evals >= config.max_evals:
                    break
                # Avoids trapping of the algorithm:
                #   allows no more than half the maximum number of evaluations for the inner loop.
                # @Sean Moved this below the `num_evals` update, which changes behaviour a bit but is more correct,
                #   since we did just evaluate `population_size` more times.
                if not looping or population_size * t > config.max_evals / 2:
                    break

            # Ranks again with `th=1`.
            buf.elite = pareto_rank(buf.elite, num_vars, num_objectives, 1)

            # Loop terminates if `max_evals` was reached before `max_loops` were executed.
            if buf.num_evals >= config.max_evals:
                break

        # Performs ranking for final time with `th=0`.
        buf.elite = pareto_rank(buf.elite, num_vars, num_objectives, 0)

        config.log.info("MOO CEM completed. Time elapsed: {0}.".format(time.time() - start_time))
        pool.close()
        pool.join()
        return buf.elite

    @staticmethod
    def _evaluate(config_path: str, check: np.ndarray):
        """Evaluates objective functions. May be ran as a process, resources will be recycled."""
        # Avoids re-loading if the process is re-used.
        if hasattr(multiprocessing.current_process(), "moocem"):
            config, pem, pem_state = multiprocessing.current_process().moocem
        else:
            from efs3.efs_config import EFSConfig
            config = EFSConfig(config_path)
            config.setup_environment()
            pem = config.load_pem()
            pem_state = pem.State(config.pem_config)
            multiprocessing.current_process().moocem = (config, pem, pem_state)

        # Override to ignore certain stations (e.g. Medupi and Kusile).
        for i in config.moo_config.ignored:
            check[i] = 0

        pem_state.reset(config.pem_config, check)
        pem.run(pem_state)
        objectives = pem.objectives(pem_state)

        return objectives

    def select_one_solution(self, elite: np.ndarray):
        """
        Returns one solution from the Pareto-optimal solutions found, or `None` if none exists.

        The solution selected is the one closest to the mean value.

        Args:
            elite: Elite matrix to choose a solution from.

        Returns:
            A solution vector, or `None` if no solutions were found.
        """
        if not len(elite):
            return None

        n = self._config.num_vars
        m = self._config.num_objectives

        mu = np.empty(m)
        for i in range(m):
            mu[i] = np.mean(elite[:, n + i])
        y = np.sum(np.abs(mu - elite[:, n:(n + m)]), axis=0)
        x = np.argmin(y)
        return elite[x, :]
