"""
EFS3 application.
"""
# Throughout this package, we refer to the following references.
#
# References:
#    @Bekker: Bekker's 2012 Matlab implementation of MOO_CEM for his PhD thesis.
#    @Brits: Brits (2016) paper.
#    @EFS2: R implementation of EFS2 MOO_CEM.
