# GIVE FUNCTION
Give <- function(TransferTemp1, PosTrans, Dist, name, policy){
  
  # r is the amount that must be given to reach target
  r <- TransferTemp1[name, 3] - TransferTemp1[name, 5]
  test <- ifelse(
    (TransferTemp1[,5]*PosTrans[,1]) <  (TransferTemp1[,3]*PosTrans[,1]), 
    (TransferTemp1[,3]*PosTrans[,1])-(TransferTemp1[,5]*PosTrans[,1]), 
    c(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0))
  test <- as.matrix(test)
  
  PosTrans <- subset(PosTrans, PosTrans[,1] == 1)
  test <- subset(test, test[,1] > 0)
  testlength <- length(test[,1])
  
  Distances <- as.matrix(Dist[rownames(test),])
  Distances <- as.matrix(sort(Distances[,1]))
  
  if (policy == 1){
    Temp <- rownames(Distances)
  }else if(policy == 2){
    Temp <- rownames(test)
  }
  
  # if the station can give
  if (testlength > 0){
     
    for (i in 1:testlength){
      
      transfer_cap <- TransferTemp1[Temp[i],8]
      
      TransferTemp1[name,6] <- -min(abs(r), test[Temp[i],1], transfer_cap)
      TransferTemp1[Temp[i], 6] <- min(abs(r), test[Temp[i],1], transfer_cap)
      
      TransferTemp1[,7] <- TransferTemp1[,5] + TransferTemp1[,6]
      TransferTemp1[,5] <- TransferTemp1[,7]
      TransferTemp1[,6] <- 0
      TransferTemp1[,7] <- 0
      
      r <- TransferTemp1[name,5] - TransferTemp1[name,3]
      
    }
    
    
    Temp2 <- matrix(TransferTemp1[rownames(test),], ncol=8)
    rownames(Temp2) <- rownames(Distances)
    colnames(Temp2) <- colnames(TransferTemp1)
    
    cost <- matrix(0, nrow=testlength, ncol=1)
    rownames(cost) <- c(rownames(Temp2))  
    cost[,1] <- c(Distances[,1]) 
    
    cost[1:testlength, 1] <- cost[1:testlength, 1] * 
      abs(Temp2[1:testlength, 5] - Temp2[1:testlength, 1])
    
  }else{
    
    cost <- matrix(0, nrow=1, ncol=1)
    cost[1,1] <- 0
    
  }
  

  returnlist <- list(TransferTemp1, cost)
  return(returnlist)
   
} # END OF GIVE FUNCTION

