# EFS v.3 Energy Flow Simulator with Multi-Objective Optimizer using Cross-Entropy Method.

See the documentation in `doc-build/html` and the [project wiki](https://bitbucket.org/snel/efs3/wiki/Home) for information.
