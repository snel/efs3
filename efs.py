"""
EFS application.

This is the main script that launches either the command-line application or the GUI.
"""
import datetime
import logging
import time

import click
import numpy as np
import pandas

from efs3.efs_config import EFSConfig
from efs3.gui import efs_gui
from efs3.moocem.moocem import MOOCEM
from efs3.pem.pem import PEM


def results_as_dataframe(elites: np.ndarray, config: EFSConfig) -> pandas.DataFrame:
    """
    Returns the `elites` array as a pandas `Dataframe` for saving to disk in the CSV format EFS2 used.

    Args:
        elites: Matrix of Pareto-optimal solutions.
        config: `EFSConfig` object.

    Returns:
        Returns the `elites` array as a pandas `DataFrame`.
    """
    # Formats output.
    m = config.moo_config.num_objectives
    num_vars = config.moo_config.num_vars
    results = np.zeros((elites.shape[0], num_vars + m))
    for i in range(0, num_vars - m, 3):
        results[:, i] = elites[:, i]
        results[:, i + 1] = elites[:, i] + elites[:, i + 1]
        results[:, i + 2] = elites[:, i] + elites[:, i + 1] + elites[:, i + 2]
    for i in range(num_vars, num_vars + m):
        results[:, i] = elites[:, i]

    df = pandas.DataFrame(results)
    stations = np.array(config.pem_config.coal.stations)
    # This removes ignored stations (e.g. Medupi and Kusile) if we called the R code.
    if config.rpem:
        stations = np.delete(stations, config.moo_config.ignored)

    cols = np.array([["{0} LWL".format(station), "{0} TARGET".format(station), "{0} UWL".format(station)]
                     for station in stations]).flatten()
    cols = np.concatenate([cols, ["f0", "f1"]])
    df.columns = cols
    return df


def load_efs(config: EFSConfig) -> (PEM, MOOCEM):
    """Returns a `PEM` and `MOOCEM` instance."""
    config.setup_environment()
    # Fail-fast: loads PEM and MOO immediately.
    # Note that we can use the same PEM and MOOCEM instances from multiple threads.
    pem = config.load_pem()
    moo = config.load_moocem(pem)
    return pem, moo


def moocem(config: EFSConfig) -> None:
    """Runs the MOO computation and displays a progress bar to the console."""
    pem, moo = load_efs(config)

    # Starts computing.
    t = time.time()
    logging.info("MOO started at {0}.".format(datetime.datetime.today()))
    with click.progressbar(length=config.moo_config.max_evals,
                           label='Computing...') as bar:
        elites = moo.run(update_progress=lambda i, n: bar.update(i))

    if config.select_single_solution and len(elites):
        elites = moo.select_one_solution(elites)[np.newaxis, :]

    # Displays and saves results.
    print("Time elapsed: {0} s.".format(time.time() - t))
    logging.info("MOO _completed. Time elapsed: {0} s.".format(time.time() - t))
    logging.info("Time {0}.".format(datetime.datetime.today()))
    results = results_as_dataframe(elites, config)
    print(results)
    results.to_csv(config.output_path)


@click.command()
@click.option('--config', default='config.json', type=click.Path(exists=True),
              help="Path to JSON configuration file. Default is \"./config.json\".")
@click.option('--gui', default=True, type=bool,
              help="Enables the GUI. Default is \"True\".")
def main(config, gui) -> None:
    """
    Multi-objective optimizer using Cross-Entropy Method (MOO CEM) for Energy Flow Simulator (EFS) version 3.00.
    """
    cfg = EFSConfig(config)
    cfg.setup_environment()

    if gui:
        efs_gui.run(cfg)
    else:
        moocem(cfg)


if __name__ == '__main__':
    main()
