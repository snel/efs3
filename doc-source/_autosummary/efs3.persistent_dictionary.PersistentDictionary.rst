efs3.persistent_dictionary.PersistentDictionary
===============================================

.. currentmodule:: efs3.persistent_dictionary

.. autoclass:: PersistentDictionary

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~PersistentDictionary.__init__
      ~PersistentDictionary.clear
      ~PersistentDictionary.close
      ~PersistentDictionary.copy
      ~PersistentDictionary.dump
      ~PersistentDictionary.fromkeys
      ~PersistentDictionary.get
      ~PersistentDictionary.items
      ~PersistentDictionary.keys
      ~PersistentDictionary.load
      ~PersistentDictionary.pop
      ~PersistentDictionary.popitem
      ~PersistentDictionary.setdefault
      ~PersistentDictionary.sync
      ~PersistentDictionary.update
      ~PersistentDictionary.values
   
   

   
   
   