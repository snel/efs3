efs3.pem.pem.PEM
================

.. currentmodule:: efs3.pem.pem

.. autoclass:: PEM

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~PEM.__init__
      ~PEM.objectives
      ~PEM.run
   
   

   
   
   