efs3.moocem.moocem.MOOCEM
=========================

.. currentmodule:: efs3.moocem.moocem

.. autoclass:: MOOCEM

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~MOOCEM.__init__
      ~MOOCEM.run
      ~MOOCEM.select_one_solution
   
   

   
   
   