efs3.gui.time_estimator.TimeEstimator
=====================================

.. currentmodule:: efs3.gui.time_estimator

.. autoclass:: TimeEstimator

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~TimeEstimator.__init__
      ~TimeEstimator.finish
      ~TimeEstimator.format_elapsed_time
      ~TimeEstimator.format_time_remaining
      ~TimeEstimator.make_step
      ~TimeEstimator.set_maximum
   
   

   
   
   .. rubric:: Attributes

   .. autosummary::
   
      ~TimeEstimator.elapsed_time
      ~TimeEstimator.percent
      ~TimeEstimator.time_per_step
      ~TimeEstimator.time_remaining
   
   