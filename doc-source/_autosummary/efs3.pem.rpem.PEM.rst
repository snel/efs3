efs3.pem.rpem.PEM
=================

.. currentmodule:: efs3.pem.rpem

.. autoclass:: PEM

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~PEM.__init__
      ~PEM.objectives
      ~PEM.run
   
   

   
   
   