efs3.moocem.moocem_config.MOOCEMConfig
======================================

.. currentmodule:: efs3.moocem.moocem_config

.. autoclass:: MOOCEMConfig

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~MOOCEMConfig.__init__
   
   

   
   
   