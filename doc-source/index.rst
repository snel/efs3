.. EFS documentation master file, created by
   sphinx-quickstart on Mon Dec  5 06:07:58 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentation for EFS 3
===============================

Author: S. E. A. Nel [5394431@gmail.com]

Date: 2016/12/05

This is the documentation for Energy Flow Simulator (EFS) implemented in Python 3. The focus of this package is multi-objective optimization applied to EFS.

.. toctree::
   
   intro
   config
   dev

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

