EFS 3 configuration
===============================

EFS 3 configuration files are assumed to be in JSON format. 
Unless otherwise specified by the user, the file that will be used is "```./config.json```". This file is located where you installed EFS. 

The config file allows the user to set parameters for MOO and PEM as well as the paths to `CSV` input files and the output `CSV` file.
The input files serve as additional configuration. These are in the same format as used in EFS 2 (the R implementation of EFS). This format was chosen to maintain compatibility as much as possible and simplify the use of EFS 3 for users familiar with EFS 2.

.. note::
   While many aspects of the application can be configured, the "uncertainty distributions" remain hard-coded. Changes to these distributions (including the addition of new stations) currently require changes to this file to update the distribution functions. See ```efs3.efs3.pem.efs2_csv_format.set_hardcoded_uncertainty_distributions()```.
   
Parameters
----------
`select_single_solution`
    Selects a single solution from among the Pareto-optimal solutions found by MOO.
`input_path`
    Path where input CSV files are located.
`output_path`
    Path where output is to be stored.
`fail_on_error`: 
    If `True`, numerical errors will cause computation to stop. Otherwise, warnings will be written to the `log.txt` file.
`moocem`

    `max_evals`
        Maximum number of evaluations.
    `max_loops`
        Maximum number of outer loops supporting the multi-objective search. In single-objective search, it would be one loop only.
    `population_size`
        Population size.
    `alpha`
        Alpha of the CEM (smoothing parameter). Set at 0.7--0.8.
    `p_h`
        Probability to invert histograms to diversify search and to avoid trapping.
    `epsilon`
        Epsilon of the CEM (common termination threshold). Set at 0.1--1.
    `ignored`
        List of stations to ignore, e.g. `[1, 8]` for Medupi and Kusile. Their decision variables will always be set to 0. If R delegation is enabled, the input vector passed to the generation function will not include these stations.
    `mock_stats`
        Enables mocking of statistical functions, to produce predictable results. For debug purposes--not recommended for general use.
    `num_processes`
        Number of processes to use for parallel computation. Must be larger than 0. Processes compute evaluation functions of individuals in parallel. Therefore, no more than `population_size` processes will be run at a time. If not set, the number of available CPU cores will be used.
    `dv_path`
        Path to decision variables CSV file.
  
`pem`

    `coal_stations`
        List of coal fired coal stations.
    `other_stations`
        List of non-coal fired coal stations.
    `no_uncertainty`
        Sets all uncertainty distributions to 0. For debug purposes--not recommended for general use.
    `mimic_efs2`
        Attempts to produce results as similar to EFS 2 as possible, but computation may be slower. For debug purposes--not recommended for general use.
    `settings`, `burn_plan_path, `delivery_plan_path`, `transfer_distances_path`, `cv_path`, `oclf_path`, `pclf_path`, `sdb_path`, `uclf_path`, `hr_path`, `possible_transfers_path`
        Paths to input CSV files.
  
`r`

    `r_pem` 
        Enables R delegation. This means that an R module will be used instead of the Python implementation of PEM.
    `r_efs_path`
        Path to R source code. This will be the working directory as seen from R.
    `r_pem_path`
        Path to R source code to substitute the PEM module.
    `r_libs_path`
        Path to R libraries. Required for R delegation to work.
    `r_user_path`
        Path to R user directory. Required for R delegation to work.

  
Config file example
-------------------
This is best illustrated with an example. In the following file, all possible settings are explicitly defined. These are the default values. If any values are missing from the file, these default values will be used instead.

.. code-block:: json

   {
      "select_single_solution": true,
      "input_path": "inputs",
      "output_path": "outputs/results.csv",
      "fail_on_error": false,
      "moocem": {
        "max_evals": 250,
        "max_loops": 20,
        "population_size": 20,
        "alpha": 0.7,
        "p_h": 0.3,
        "epsilon": 0.1,
        "ignored": [],
        "mock_stats": false,
        "num_processes": 8,
        "dv_path": "inputs/DecisionVariableRanges.csv"
      },
      "pem": {
        "coal_stations": ["Matimba", "Medupi", "Lethabo", "Duvha", "Hendrina", "Matla", "Kriel1_3", "Kriel4_6", "Kusile",
                 "Kendal", "Majuba", "Tutuka", "Camden", "Arnot", "Komati", "Grootvlei"],
        "other_stations": ["CahoraBassa", "IPPs", "Koeberg", "Ankerlig", "Gourikwa", "Acacia", "PortRex", "Unmet", "Gariep",
                  "Vanderkloof", "Drakensberg", "Palmiet", "Ingula"],
        "no_uncertainty": false,
        "mimic_efs2": false,
        "settings": "Settings.csv",
        "burn_plan_path": "inputs/BurnPlan_daily.csv",
        "delivery_plan_path": "inputs/DeliveryPlan_daily.csv",
        "transfer_distances_path": "inputs/DistanceMatrix.csv",
        "cv_path": "inputs/Planned_CV.csv",
        "oclf_path": "inputs/Planned_OCLF.csv",
        "pclf_path": "inputs/Planned_PCLF.csv",
        "sdb_path": "inputs/Planned_SDB.csv",
        "uclf_path": "inputs/Planned_UCLF.csv",
        "hr_path": "inputs/PlannedHeatRates.csv",
        "possible_transfers_path": "inputs/TransfersMatrix.csv"
      },
      "r": {
        "r_pem": false,
        "r_efs_path": "efs2",
        "r_pem_path": "efs2/PEM.R",
        "r_libs_path": "C:/Users/Sean/Documents/R/win-library/3.3",
        "r_user_path": "C:/Users/Sean/Documents"
      }
   }


CSV files and station ordering
------------------------------

Please note that the order that data appear in the CSV files are important and must correspond to the order given in `coal_stations` and `other_stations`.

Also note that the total order of *all* the stations is assumed to be:

#. CBASSA,
#. IPPS,
#. Koeberg,
#. Matimba,
#. Medupi,
#. Lethabo,
#. Duvha,
#. Hendrina,
#. Matla,
#. Kriel 1_3,
#. Kriel 4_6,
#. Kusile,
#. Kendal,
#. Majuba,
#. Tutuka,
#. Camden,
#. Arnot,
#. Komati,
#. Grootvlei,
#. Ankerlig,
#. Gourikwa,
#. Acacia,
#. Port Rex,
#. Unmet,
#. Gariep,
#. Vanderkloof,
#. Drakensberg,
#. Palmiet,
#. Ingula,
#. Drak_Pump,
#. Palm_Pump,
#. Ingula-Pump.

This is the order that they must appear in ```Planned_OCLF.csv``` and ```Planned_UCLF.csv```, because that is how they appeared in EFS 2.

R delegation
-------------------
It is possible to run the R implementation of ```PEM.R``` with EFS 3 instead of the Python implementation. See the configuration parameters under the ```r``` section to activate this.

.. note::
   It is impossible to control hard-coded settings in the R source code from EFS 3. In particular, be careful to distinguish the input path ```PEM.R``` uses from the one EFS 3 uses. For example, R files provided in the ```./efs2``` directory use the CSV files defined in ```./efs2/inputs``` and NOT the ones in ```./inputs```. 